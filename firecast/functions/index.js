'use strict'

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

exports.sendMessageNotification = functions.database.ref('/CommentNotifications/{user_id}/{notification_id}')
  .onWrite((change, context) => {
    // console.log("payload ==> ", JSON.stringify(context));

    const data_context = context.params;
    const user_id = data_context.user_id;
    const notification_id = data_context.notification_id;

    console.log('We have a notification ==> ' + user_id);
    admin.database().ref(`/CommentNotifications/${user_id}/${notification_id}`).on(`value`, (fromUserResult) => {
      // console.log("fromUser is ", fromUserResult);

      const senderId = fromUserResult.val().senderId;
      const name = fromUserResult.val().name;
      const message = fromUserResult.val().message;
      const ticketId = fromUserResult.val().ticketId;
      const type = fromUserResult.val().type;

      console.log('You have new notification from  ==> ' + senderId + " " + name + " " + ticketId);

      // const userQuery = admin.database().ref('Users/${from_user_id}/name').once('value');
      let userTokensRef = admin.database().ref(`/Tokens/`);
      userTokensRef.on(`value`, (tokensList) => {
        tokensList.forEach((tokenNode) => {
          const token_id = tokenNode.val().token;
          console.log('You Device Token is ==> ' + token_id);

          const payload = {w
            data: {
              senderId: senderId,
              name: name,
              message: message,
              ticketId: ticketId,
              type : type,
              icon: "default",
              sound: "default",
              click_action: "com.example.ttescalation.SplashScreenActivity"
            }
          };
          /*
           * Using admin.messaging() we are sending the payload notification to the token_id of
           * the device we retreived.
           */
          admin.messaging().sendToDevice(token_id, payload);
        });

        setTimeout(() => {
          console.log("Final Step ==> Deleted");
          var adaRef = admin.database().ref(`CommentNotifications/${user_id}`);
          adaRef.remove();
        }, 100);

        //#region 
        // return dt.then(async token => {
        // return await Promise(async (resolve, reject) => {
        //   const token_id = token.val().token;
        //   const payload = {
        //     data: {
        //       senderId: senderId,
        //       name: name,
        //       message: message,
        //       ticketId: ticketId,
        //       icon: "default",
        //       sound: "default",
        //       click_action: "com.example.ttescalation.SplashScreenActivity"
        //     }
        //   };

        //   /*
        //    * Using admin.messaging() we are sending the payload notification to the token_id of
        //    * the device we retreived.
        //    */
        //   var adaRef = admin.database().ref(`CommentNotifications/${user_id}`);
        //   adaRef.remove();
        //   return resolve(admin.messaging().sendToDevice(token_id, payload));
        // });
        // })
        //#endregion

        //#region 
        //   return Promise.all([deviceToken]).then(result => {
        //     const token_id = result[0].val();
        //     const payload = {
        //       data: {
        //         senderId: senderId,
        //         name: name,
        //         message: message,
        //         ticketId : ticketId,
        //         icon: "default",
        //         sound: "default",
        //         click_action: "com.example.ttescalation.SplashScreenActivity"
        //       }
        //     };

        //     /*
        //      * Using admin.messaging() we are sending the payload notification to the token_id of
        //      * the device we retreived.
        //      */
        //     var adaRef = admin.database().ref(`CommentNotifications/${user_id}`);
        //     adaRef.remove();
        //     return admin.messaging().sendToDevice(token_id, payload);
        //   });
        //#endregion
      });
    });
  });