package com.example.ttescalation.Activities;

import android.app.Dialog;
import android.app.Presentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ttescalation.Adapters.DrawerAdapter;
import com.example.ttescalation.Fragments.AddTaskFragment;
import com.example.ttescalation.Fragments.AddTicketFragment;
import com.example.ttescalation.Fragments.DashboardFragment;
import com.example.ttescalation.Fragments.HistoryFragment;
import com.example.ttescalation.Fragments.PreSalesDashboardFragment;
import com.example.ttescalation.Fragments.PreSalesHistory;
import com.example.ttescalation.Fragments.ServiceTypeFragment;
import com.example.ttescalation.Model.DrawerModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.SendNotificationPack.Token;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class DrawerActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView backIv, menuIv;
    public static ImageView logoutIv;

    TextView nameTv, userNameTv, designationTv;
    public static TextView fragmentName;

    DrawerLayout drawer;
    NavigationView navigationView;
    ListView listView;

    UserModel userModel;
    List<DrawerModel> drawerModelList = new ArrayList<>();
    DrawerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        SystemUtils.setActivity(this);


        setXml();

        if (savedInstanceState == null) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nav_host_fragment, new DashboardFragment(), "Dashboard Fragment")
                    .commit();

        }

    }

    void setXml() {
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        listView = findViewById(R.id.list_slidermenu);

        menuIv = findViewById(R.id.menuIv);

        menuIv.setOnClickListener(this);
        menuIv.setVisibility(View.VISIBLE);
        fragmentName = findViewById(R.id.nameTv);

        backIv = findViewById(R.id.backIv);
        backIv.setVisibility(View.GONE);
        backIv.setOnClickListener(this);

        logoutIv = findViewById(R.id.logoutIv);
//        logoutIv.setVisibility(View.VISIBLE);
        logoutIv.setOnClickListener(this);

        nameTv = findViewById(R.id.nameTv);
        nameTv.setText("Dashboard");

        View headerView = navigationView.getHeaderView(0);

        userNameTv = headerView.findViewById(R.id.userNameTv);
        designationTv = headerView.findViewById(R.id.designationTv);

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);

        userNameTv.setText(userModel.getName());
        designationTv.setText(userModel.getDesignation());

        initDrawerItems();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIv:
                handleDrawer();
                break;
            case R.id.logoutIv:
                showDialog();
                break;
        }
    }

    void handleDrawer() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer((int) Gravity.LEFT);
        } else {
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    protected void onResume() {
        SystemUtils.setActivity(this);

//        if (getForegroundFragment() instanceof DashboardFragment)
//            navigationView.getMenu().getItem(0).setChecked(true);
//        else if (getForegroundFragment() instanceof AddTicketFragment)
//            navigationView.getMenu().getItem(1).setChecked(true);

        super.onResume();
    }

    public Fragment getForegroundFragment() {
        Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        return navHostFragment == null ? null : navHostFragment.getChildFragmentManager().getFragments().get(0);
    }

    public boolean getHistoryFragment() {
        if (getSupportFragmentManager().findFragmentByTag("HistoryFragment") instanceof PreSalesHistory)
            return false;
        else
            return true;
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.logout_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        TextView ok = dialog.findViewById(R.id.ok);
        TextView cancel = dialog.findViewById(R.id.cancel);

        dialog.show();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                logoutApi();
                new SystemPrefs(SystemUtils.getActivity()).saveLogin(false);

                SharedPreferences settings = SystemUtils.getActivity()
                        .getSharedPreferences("ttescalation", Context.MODE_PRIVATE);
                settings.edit().clear().commit();

//                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
//                Token token1 = new Token(token);
//                reference.child(userModel.getId()).setValue("");

                FirebaseAuth.getInstance().signOut();

                startActivity(new Intent(SystemUtils.getActivity(), LoginActivity.class));
                finishAffinity();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    void initDrawerItems() {

        drawerModelList = new ArrayList<>();
        String[] names = {"Dashboard", "Generate Ticket", "History", "Tickets by Services"
//                , "Ticekts by Issues" ,
//                , "Pre-Sales", "Add Task"
        };
        Drawable[] drawables = {getDrawable(R.drawable.ic_dashboard), getDrawable(R.drawable.ic_validating_ticket),
                getDrawable(R.drawable.ic_history), getDrawable(R.drawable.ic_services)
//                getDrawable(R.drawable.ic_issue)
//                getDrawable(R.drawable.ic_presales), getDrawable(R.drawable.ic_checklists)
        };


        for (int i = 0; i < names.length; i++) {
            DrawerModel drawerModel = new DrawerModel();
            drawerModel.setName(names[i]);
            drawerModel.setPicture(drawables[i]);
            drawerModelList.add(drawerModel);
        }
        adapter = new DrawerAdapter(drawerModelList, SystemUtils.getActivity());
        listView.setAdapter(adapter);

//        listView.getChildAt(0).setBackgroundColor(ContextCompat.getColor(SystemUtils.getActivity(), R.color.zong_default_color));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    fragmentName.setText("Dashboard");
                    replaceFragment(new DashboardFragment());
                    drawer.closeDrawer(Gravity.LEFT);
//                        SystemUtils.showCustomToast("Under development", SystemUtils.getActivity());
                } else if (position == 1) {
                    if (userModel.isAdmin()) {
                        fragmentName.setText("Add Ticket");
                        replaceFragment(new AddTicketFragment());
                    } else
                        Toast.makeText(SystemUtils.getActivity(), "Dear " + userModel.getName() + " You don't have rights to see ticket history", Toast.LENGTH_LONG).show();

                    drawer.closeDrawer(Gravity.LEFT);

                } else if (position == 2) {

                    if (userModel.isAdmin()) {
                        fragmentName.setText("Tickets History");
                        Bundle bundle = new Bundle();
                        bundle.putString("Type", Constants.ALL_REGIONS);
                        bundle.putString("Status", "null");
                        bundle.putString("direction", Constants.DashboardSide);

                        HistoryFragment historyFragment = new HistoryFragment();
                        historyFragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.nav_host_fragment, historyFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        Toast.makeText(SystemUtils.getActivity(), "Dear " + userModel.getName() + " You don't have rights to see ticket history", Toast.LENGTH_LONG).show();
                    }
                    drawer.closeDrawer(Gravity.LEFT);

                } else if (position == 3) {
                    if (userModel.isAdmin()) {
                        fragmentName.setText("Tickets against Services");
                        replaceFragment(new ServiceTypeFragment());
                    }
//                    Toast.makeText(SystemUtils.getActivity(), "Under development ", Toast.LENGTH_SHORT).show();
                    drawer.closeDrawer(Gravity.LEFT);
                }
//                else if (position == 4) {
//                    Toast.makeText(SystemUtils.getActivity(), "Under development ", Toast.LENGTH_SHORT).show();
//                    drawer.closeDrawer(Gravity.LEFT);
//                }
                else if (position == 4) {
//                    Toast.makeText(SystemUtils.getActivity(), "Under development ", Toast.LENGTH_SHORT).show();
                    fragmentName.setText("Pre-sales Dashboard");
                    replaceFragment(new PreSalesDashboardFragment());
                    drawer.closeDrawer(Gravity.LEFT);
                } else if (position == 5) {
                    if (userModel.isAdmin()) {
                        fragmentName.setText("Add Tasks");
                        replaceFragment(new AddTaskFragment());
                    }else {
                        Toast.makeText(SystemUtils.getActivity(), "Dear " + userModel.getName() + " You don't have rights to add tasks history", Toast.LENGTH_LONG).show();
                    }
                    drawer.closeDrawer(Gravity.LEFT);
                }

                for (int i = 0; i < listView.getChildCount(); i++) {
                    if (position == i) {
                        listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(SystemUtils.getActivity(), R.color.zong_default_color));
                    } else {
                        listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(SystemUtils.getActivity(), R.color.light_gray));
                    }
                }

            }
        });
//        listView.performItemClick(null, 0, listView.getItemIdAtPosition(0));
    }

    void replaceFragment(Fragment newFragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
//        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.LEFT))
            drawer.closeDrawer(Gravity.LEFT);
        else if (getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof AddTicketFragment
                || getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof HistoryFragment
                || getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof ServiceTypeFragment
                || getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof PreSalesDashboardFragment
                || getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof AddTaskFragment
                || getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment) instanceof PreSalesHistory) {
            replaceFragment(new DashboardFragment());
            for (int i = 0; i < listView.getChildCount(); i++) {
                if (i == 0)
                    listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(SystemUtils.getActivity(), R.color.zong_default_color));
                else
                    listView.getChildAt(i).setBackgroundColor(ContextCompat.getColor(SystemUtils.getActivity(), R.color.light_gray));
            }
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }

    }
    public Fragment getVisibleFragment(){
        FragmentManager fragmentManager = DrawerActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }
}
