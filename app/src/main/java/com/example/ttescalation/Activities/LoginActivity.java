package com.example.ttescalation.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailEt, passwordEt;
    RelativeLayout loginRl;

    AVLoadingIndicatorView avi;
    ImageView backIv;
    TextView nameTv, forgotTv, signupTv;

    FirebaseAuth firebaseAuthLogin;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SystemUtils.setActivity(this);
        setXml();

//        createUserFromFireBase("usman.mazhar@zong.com.pk" , "123456");
//        addUserToUserTable("usman.mazhar@zong.com.pk" , "123456" , false ,
//                "Usman Ahmed Mazhar" , "C B S E - Central");

    }

    void setXml() {
        avi = findViewById(R.id.avi);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        loginRl = findViewById(R.id.loginRl);
        signupTv = findViewById(R.id.signupTv);

        loginRl.setOnClickListener(this);
        signupTv.setOnClickListener(this);

        forgotTv = findViewById(R.id.forgotTv);
        forgotTv.setOnClickListener(this);

        backIv = findViewById(R.id.backIv);
        backIv.setVisibility(View.GONE);
        backIv.setOnClickListener(this);
        nameTv = findViewById(R.id.nameTv);
        nameTv.setText("Login");

        firebaseAuthLogin = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("Users");

        focusedInputLayouts();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginRl:
                if (!verifyEmail(emailEt.getText().toString().trim())) {
                    Toast.makeText(LoginActivity.this, "Invalid email", Toast.LENGTH_LONG).show();
                    return;
                } else if (passwordEt.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
                    return;
                }

//                loginApi();
                loginUserWithFirebase(emailEt.getText().toString().trim(), passwordEt.getText().toString().trim());
                break;
            case R.id.backIv:
                onBackPressed();
                break;
            case R.id.forgotTv:
//                startActivity(new Intent(SystemUtils.getActivity(),ForgotPassActivity.class));
                break;
            case R.id.signupTv:
//                startActivity(new Intent(SystemUtils.getActivity(), SignUpActivity.class));
                break;
        }
    }


    boolean verifyEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    protected void onPause() {
        stopAnim();
        super.onPause();
    }


    @Override
    protected void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        SystemUtils.setActivity(this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void loginUserWithFirebase(final String email, final String password) {
        startAnim();
        firebaseAuthLogin.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(SystemUtils.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        stopAnim();

                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuthLogin.getCurrentUser();
//                            Log.d("responseId", user.getDisplayName());
                            Log.d("responseId", user.getEmail());

                            mDatabase.orderByChild("email").equalTo(email)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            stopAnim();
                                            if (!dataSnapshot.exists()) {
                                                Toast.makeText(SystemUtils.getActivity(), "You have entered wrong credentials ", Toast.LENGTH_LONG).show();
                                                return;
                                            }
                                            for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                                                Log.d("userInfo", userSnapshot.getKey());
                                                Log.d("userInfo", userSnapshot.child("name").getValue(String.class));

                                                if (!password.equals(userSnapshot.child("password").getValue().toString())) {
                                                    Toast.makeText(SystemUtils.getActivity(), "Enter wrong password ", Toast.LENGTH_LONG).show();
                                                    return;
                                                }
                                                UserModel userModel = new UserModel(
                                                        userSnapshot.child("id").getValue(String.class),
                                                        userSnapshot.child("email").getValue(String.class),
                                                        userSnapshot.child("password").getValue(String.class),
                                                        userSnapshot.child("admin").getValue(Boolean.class),
                                                        userSnapshot.child("name").getValue(String.class),
                                                        userSnapshot.child("designation").getValue(String.class), "",
                                                        userSnapshot.child("typing").getValue(Boolean.class));

                                                new SystemPrefs(SystemUtils.getActivity()).setObjectData(Constants.USER, (Object) userModel);
                                                new SystemPrefs(SystemUtils.getActivity()).saveLogin(true);

                                                Toast.makeText(SystemUtils.getActivity(), "Successfully Login ...!", Toast.LENGTH_LONG).show();
                                                startActivity(new Intent(SystemUtils.getActivity(), DrawerActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                                                finishAffinity();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
//                                            throw databaseError.toException();
                                            Toast.makeText(SystemUtils.getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                        } else {
                            stopAnim();
                            Log.d("Exception" , task.getException().toString());
                            Toast.makeText(SystemUtils.getActivity(), "" + task.getException().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    void createUserFromFireBase(String email, String password) {
        final FirebaseAuth mAuth;
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("response", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("response", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SystemUtils.getActivity(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void addUserToUserTable(String email, String password, boolean isAdmin, String name, String designation) {
        startAnim();

        final String key = mDatabase.push().getKey();

        UserModel userModel = new UserModel();
        userModel.setIsAdmin(isAdmin);
        userModel.setEmail(email);
        userModel.setId(key);
        userModel.setPassword(password);
        userModel.setName(name);
        userModel.setTyping(false);
        userModel.setDesignation(designation);

//        UserModel listdata = new UserModel(key, email, password, isAdmin);

        mDatabase.child(key).setValue(userModel).
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        stopAnim();

                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "User Added Successfully !", Toast.LENGTH_SHORT).show();


//                            startActivity(new Intent(SystemUtils.getActivity(), PieChartActivity.class));

                        } else if (task.isCanceled())
                            Toast.makeText(LoginActivity.this, "User addition failed !", Toast.LENGTH_SHORT).show();

//                        startActivity(new Intent(getApplicationContext(),HomeScreen.class));
                    }
                });

    }

    void focusedInputLayouts() {
        emailEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    emailEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                } else
                    emailEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
        passwordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    passwordEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                } else
                    passwordEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
    }

}
