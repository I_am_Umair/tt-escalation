package com.example.ttescalation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Activities.LoginActivity;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;

public class SplashActivity extends AppCompatActivity {

    SystemPrefs systemPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SystemUtils.setActivity(this);
        systemPrefs = new SystemPrefs(SystemUtils.getActivity());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (systemPrefs.isLogin()){
                    startActivity(new Intent(SystemUtils.getActivity(), DrawerActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                }else {
                    startActivity(new Intent(SystemUtils.getActivity(), LoginActivity.class));
                }
                finish();
            }
        }, 1500);
    }
}
