package com.example.ttescalation.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentProviderClient;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.core.annotations.Line;
import com.example.ttescalation.Adapters.MessageAdapter;

import com.example.ttescalation.Model.CommentModel;
import com.example.ttescalation.Model.NotificationModel;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.TaskModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.SendNotificationPack.APIService;
import com.example.ttescalation.SendNotificationPack.Client;
import com.example.ttescalation.SendNotificationPack.Data;
import com.example.ttescalation.SendNotificationPack.MyResponse;
import com.example.ttescalation.SendNotificationPack.NotificationSender;
import com.example.ttescalation.SendNotificationPack.Token;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.RecyclerViewHandler;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CommentsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView nameTv;
    ImageView backIv, detailsIv;

    RecyclerView recycler_view;
    AVLoadingIndicatorView avi;

    String ticketId, ticketDetails;
    String type = "";
    private DatabaseReference mDatabase;

    TTModel ticketObject = null;
    TaskModel taskObject = null;
    MessageAdapter messageAdapter;
    ImageButton btn_send;
    EditText text_send;
    ImageView musicIv;
    UserModel userModel;
    List<CommentModel> commentModelList = new ArrayList<>();
    RecyclerView.LayoutManager mLayoutManager;
    TextView typingTv;

    List<UserModel> userModelList;

    APIService apiService;
    List<Token> tokenList = new ArrayList<>();

    private static String MUSIC_OFF = "MusicOff";
    private static String MUSIC_ON = "MusicOn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        SystemUtils.setActivity(this);

        setXml();

//        text_send.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() > 0) {
//                    userModel.setTyping(true);
//                    new SystemPrefs(SystemUtils.getActivity()).setObjectData(Constants.USER, (Object) userModel);
//                    FirebaseDatabase.getInstance().getReference(Constants.USERS_TABLE)
//                            .child(userModel.getId()).setValue(userModel);
//
//                } else if (s.length() == 0) {
//                    userModel.setTyping(false);
//                    new SystemPrefs(SystemUtils.getActivity()).setObjectData(Constants.USER, (Object) userModel);
//                    FirebaseDatabase.getInstance().getReference(Constants.USERS_TABLE)
//                            .child(userModel.getId()).setValue(userModel);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

    void setXml() {
        avi = findViewById(R.id.avi);
        detailsIv = findViewById(R.id.detailsIv);
        backIv = findViewById(R.id.backIv);
        backIv.setOnClickListener(this);
        detailsIv.setOnClickListener(this);
        nameTv = findViewById(R.id.nameTv);
        nameTv.setText("Comments Section");
        btn_send = findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
        text_send = findViewById(R.id.text_send);
        musicIv = findViewById(R.id.musicIv);
        typingTv = findViewById(R.id.typingTv);

        detailsIv.setVisibility(View.VISIBLE);
        musicIv.setVisibility(View.VISIBLE);
        musicIv.setOnClickListener(this);
//        musicIv.setTag(MUSIC_ON);

        if (new SystemPrefs(SystemUtils.getActivity()).isMusic()) {
            musicIv.setBackground(getDrawable(R.drawable.ic_music));
        } else
            musicIv.setBackground(getDrawable(R.drawable.ic_music_off));

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);


        recycler_view = findViewById(R.id.recyclerView);

        recycler_view.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(SystemUtils.getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemViewCacheSize(40);

        ticketId = getIntent().getStringExtra("id");
        type = getIntent().getStringExtra("type");
        Log.d("idActivity", ticketId);
//        ticketDetails = getIntent().getStringExtra("ticketDetails");
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TICKET_TABLE);

        commentModelList = new ArrayList<>();
        messageAdapter = new MessageAdapter(CommentsActivity.this, commentModelList, userModel.getId());
        recycler_view.setAdapter(messageAdapter);

//        getAllTokens();
//        if (type.equals(Constants.TICKET_TABLE))
        getCommentsFromTicket(ticketId);
        getUsers();
//            getOnyCommentsFromTicket(ticketId);
//        else
//            getCommentsFromTask(ticketId);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backIv:
                startActivity(new Intent(SystemUtils.getActivity(), DrawerActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                finishAffinity();
                break;
            case R.id.btn_send:
                String msg = text_send.getText().toString().trim();
                if (!msg.equals("")) {
                    sendMessage(userModel.getId(), msg, SystemUtils.getTimeStamp());
                } else {
                    Toast.makeText(CommentsActivity.this, "You can't send empty message", Toast.LENGTH_SHORT).show();
                }
                text_send.setText("");
                break;
            case R.id.detailsIv:
//                if (type.equals(Constants.TICKET_TABLE)) {
                if (ticketObject != null)
                    showDetailDialog(ticketObject);
                else
                    Toast.makeText(SystemUtils.getActivity(), "Ticket id is not valid", Toast.LENGTH_LONG).show();
//                }else {
//                    if (taskObject != null)
//                        showDetailDialog(taskObject);
//                    else
//                        Toast.makeText(SystemUtils.getActivity(), "Task id is not valid", Toast.LENGTH_LONG).show();
//                }
                break;
            case R.id.musicIv:
                if (!(new SystemPrefs(SystemUtils.getActivity()).isMusic())) {
                    musicIv.setBackground(getDrawable(R.drawable.ic_music));
                    musicIv.setTag(MUSIC_ON);
                    new SystemPrefs(SystemUtils.getActivity()).saveMusic(true);
                } else if (new SystemPrefs(SystemUtils.getActivity()).isMusic()) {
                    musicIv.setTag(MUSIC_OFF);
                    musicIv.setBackground(getDrawable(R.drawable.ic_music_off));
                    new SystemPrefs(SystemUtils.getActivity()).saveMusic(false);
                }
                break;
        }
    }

    void showDetailDialog(TTModel ttModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.detail_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        EditText ticketIDEt = dialog.findViewById(R.id.ticketIDEt);
        EditText ticketCreatorEt = dialog.findViewById(R.id.ticketCreatorEt);
        EditText dateTimeEt = dialog.findViewById(R.id.dateTimeEt);
        EditText ticketDetailEt = dialog.findViewById(R.id.ticketDetailEt);
        EditText clientEt = dialog.findViewById(R.id.clientEt);
        EditText ticketTypeET = dialog.findViewById(R.id.ticketTypeET);
        EditText ticketRegionEt = dialog.findViewById(R.id.ticketRegionEt);
        EditText ticketStatusEt = dialog.findViewById(R.id.ticketStatusEt);
        EditText ettrEt = dialog.findViewById(R.id.ettrEt);

        ticketIDEt.setText(ttModel.getId());
        ticketCreatorEt.setText(ttModel.getCreator());
        dateTimeEt.setText(getDate(ttModel.getTimeStamp()));
        ticketDetailEt.setText(ttModel.getTicketDetails());
        clientEt.setText(ttModel.getClientName());
        ticketTypeET.setText(ttModel.getServiceType());
        ticketRegionEt.setText(ttModel.getRegion());
        ticketStatusEt.setText(ttModel.getStatus());
        ettrEt.setText(ttModel.getEttr());

        dialog.show();

    }

    void showDetailDialog(TaskModel ttModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.pre_sales_detail_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        EditText taskIDEt = dialog.findViewById(R.id.taskIDEt);
        EditText taskNameEt = dialog.findViewById(R.id.taskNameEt);
        EditText taskDetailsEt = dialog.findViewById(R.id.taskDetailsEt);
        EditText dateTimeEt = dialog.findViewById(R.id.dateTimeEt);
        EditText ownerEt = dialog.findViewById(R.id.ownerEt);
        EditText taskStatusEt = dialog.findViewById(R.id.taskStatusEt);


        taskIDEt.setText(ttModel.getId());
        taskNameEt.setText(ttModel.getName());
        dateTimeEt.setText(getDate(ttModel.getStartTime()));
        taskDetailsEt.setText(ttModel.getDetails());
        ownerEt.setText(ttModel.getOwner());
        taskStatusEt.setText(ttModel.getStatus());

        dialog.show();

    }


    private String getDate(long time_stamp_server) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatter.format(time_stamp_server);
    }

    void sendMessage(final String senderId, final String msg, long timeStamp) {

        if (ticketObject != null) {

//            if (type.equals(Constants.TICKET_TABLE))
            mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TICKET_TABLE);
//            else
//                mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TASK_TABLE);

//            if (type.equals(Constants.TICKET_TABLE)) {
            CommentModel commentModel = new CommentModel(senderId, userModel.getName(), msg, timeStamp);
            commentModelList.add(commentModel);
            ticketObject.setComments(commentModelList);

            mDatabase.child(ticketObject.getId()).setValue(ticketObject).
                    addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful()) {
                                messageAdapter.notifyDataSetChanged();
                                recycler_view.scrollToPosition(recycler_view.getAdapter().getItemCount() - 1);

//                            for (int i = 0; i < tokenList.size(); i++) {
                                NotificationModel notificationData = new NotificationModel(userModel.getId(), msg,
                                        userModel.getName(), ticketId, Constants.TICKET_TABLE);
                                DatabaseReference notiTable = FirebaseDatabase.getInstance().getReference("CommentNotifications");
                                notiTable.child(userModel.getId()).push().setValue(notificationData);
                            }
//                            }
                        }
                    });
//            }
//            else {
//                CommentModel commentModel = new CommentModel(senderId, userModel.getName(), msg, timeStamp);
//                commentModelList.add(commentModel);
//                taskObject.setComments(commentModelList);
//
//                mDatabase.child(taskObject.getId()).setValue(taskObject).
//                        addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//
//                                messageAdapter.notifyDataSetChanged();
//                                recycler_view.scrollToPosition(recycler_view.getAdapter().getItemCount() - 1);

//                            for (int i = 0; i < tokenList.size(); i++) {
//                                NotificationModel notificationData = new NotificationModel(userModel.getId(), msg,
//                                        userModel.getName(), ticketId , Constants.TASK_TABLE);
//                                DatabaseReference notiTable = FirebaseDatabase.getInstance().getReference("CommentNotifications");
//                                notiTable.child(userModel.getId()).push().setValue(notificationData);
//                            }
//                            }
//                        });
//            }
        } else {
            Toast.makeText(SystemUtils.getActivity(), "Ticket didnot found ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        SystemUtils.setActivity(this);
        super.onResume();
    }

    void getCommentsFromTicket(final String id) {
        startAnim();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TICKET_TABLE);
        mDatabase.orderByChild("id").equalTo(id)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        stopAnim();
                        if (!dataSnapshot.exists()) {
                            Toast.makeText(SystemUtils.getActivity(), "This ticket doesnot exist ", Toast.LENGTH_LONG).show();
                            return;
                        }
//                        List<CommentModel> newCommentList = new ArrayList<>();
//                        newCommentList.add(dataSnapshot.getValue(CommentModel.class));
//                        Log.d("SizeofList", String.valueOf(newCommentList.get(0).getMessage()));

                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            ticketObject = userSnapshot.getValue(TTModel.class);
                            Log.d("userInfoKey", userSnapshot.getKey());
                            Log.d("userInfoName", ticketObject.getClientName());
                        }
                        if (ticketObject != null) {
                            if (ticketObject.getComments() != null) {
                                commentModelList = ticketObject.getComments();
                                messageAdapter = new MessageAdapter(CommentsActivity.this, commentModelList, userModel.getId());
                                recycler_view.setAdapter(messageAdapter);
                                recycler_view.scrollToPosition(recycler_view.getAdapter().getItemCount() - 1);
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        stopAnim();
//                                            throw databaseError.toException();
                        Toast.makeText(SystemUtils.getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void getUsers() {
        startAnim();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.USERS_TABLE);
        mDatabase.orderByChild("typing").equalTo(true).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userModelList = new ArrayList<>();
                stopAnim();
                if (!dataSnapshot.exists()) {
//                            Toast.makeText(SystemUtils.getActivity(), "No user exist ", Toast.LENGTH_LONG).show();
                    return;
                }

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    UserModel user = userSnapshot.getValue(UserModel.class);
                    Log.d("userInfoKey", userSnapshot.getKey());
                    Log.d("userInfoName", userModel.getName());

                    if (!(user.getId().equals(userModel.getId()))) {
//                                if (user.isTyping())
                        userModelList.add(user);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                stopAnim();
//                                            throw databaseError.toException();
                Toast.makeText(SystemUtils.getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        if (userModelList != null && userModelList.size() >= 0) {
            typingTv.setVisibility(View.VISIBLE);
            typingTv.setText(userModelList.get(0).getName() + " is typing");
        } else
            typingTv.setVisibility(View.GONE);
    }

    void getOnyCommentsFromTicket(final String id) {
        startAnim();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TICKET_TABLE);
        mDatabase.orderByChild("id").equalTo(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        stopAnim();
                        if (!dataSnapshot.exists()) {
                            Toast.makeText(SystemUtils.getActivity(), "This ticket doesnot exist ", Toast.LENGTH_LONG).show();
                            return;
                        }
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
//                            ticketObject = userSnapshot.getValue(TTModel.class);
                            Log.d("userInfoKeys", userSnapshot.getKey());
//                            Log.d("userInfoName", userSnapshot.child("name"));
                        }
//                        if (ticketObject != null) {
//                            if (ticketObject.getComments() != null) {
//                                commentModelList = ticketObject.getComments();
//                                messageAdapter = new MessageAdapter(CommentsActivity.this, commentModelList, userModel.getId());
//                                recycler_view.setAdapter(messageAdapter);
//                                recycler_view.scrollToPosition(recycler_view.getAdapter().getItemCount() - 1);
//                            }
//                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        stopAnim();
//                                            throw databaseError.toException();
                        Toast.makeText(SystemUtils.getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void getCommentsFromTask(final String id) {
        startAnim();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.TASK_TABLE);

        mDatabase.orderByChild("id").equalTo(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        stopAnim();
                        if (!dataSnapshot.exists()) {
                            Toast.makeText(SystemUtils.getActivity(), "This task doesnot exist ", Toast.LENGTH_LONG).show();
                            return;
                        }
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            taskObject = userSnapshot.getValue(TaskModel.class);
                            Log.d("userInfoKey", userSnapshot.getKey());
                            Log.d("userInfoName", taskObject.getName());
                        }
                        if (ticketObject != null) {
                            if (ticketObject.getComments() != null) {
                                commentModelList = taskObject.getComments();
                                messageAdapter = new MessageAdapter(CommentsActivity.this, commentModelList, userModel.getId());
                                recycler_view.setAdapter(messageAdapter);
                                recycler_view.scrollToPosition(recycler_view.getAdapter().getItemCount() - 1);
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        stopAnim();
//                                            throw databaseError.toException();
                        Toast.makeText(SystemUtils.getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    protected void onPause() {
        stopAnim();
        super.onPause();
    }


    @Override
    protected void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(isTaskRoot()){
            startActivity(new Intent(SystemUtils.getActivity(), DrawerActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
            finishAffinity();
        } else
            super.onBackPressed();
    }

    //    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        String newIntent = intent.getStringExtra("id");
//        Log.d("newIntent" , newIntent);
//    }
}