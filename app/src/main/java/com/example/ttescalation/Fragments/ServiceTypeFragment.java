package com.example.ttescalation.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceTypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceTypeFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ServiceTypeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ServiceTypeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ServiceTypeFragment newInstance(String param1, String param2) {
        ServiceTypeFragment fragment = new ServiceTypeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private static final String NORTH = "North";
    private static final String CENTRAL = "Central";
    private static final String SOUTH = "South";
    private static final String ALL_REGIONS = "All Regions";

    private DatabaseReference mDatabase;
    List<TTModel> ticketsList = new ArrayList<>();
    List<TTModel> openList = new ArrayList<>();
    List<TTModel> closedList = new ArrayList<>();

    TextView totalTv, diaTv, dplcTv, priTv, ftthTv, iplcTv, ipsecTv, m2mTv, m2mATv, sip_priTv;
    TextView totalTextTv, diaTextTv, dplcTextTv, priTextTv, ftthTextTv, iplcTextTv, ipsecTextTv, m2mTextTv, m2mATextTv, sipPriTextTv;

    AVLoadingIndicatorView avi;
    Spinner regionSpinner;
    AnyChartView anyChartView;
    Pie pie;
    String regionName = "";

    int diaCount = 0, dplcCount = 0, priCount = 0, ftthCount = 0, iplcCount = 0,
            ipsecCount = 0, m2mCount = 0, m2mACount = 0, sipPriCount = 0, totalCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_type, container, false);

        setXml(view);
        return view;
    }

    void setXml(View view) {
        avi = view.findViewById(R.id.avi);
        regionSpinner = view.findViewById(R.id.regionSpinner);
        anyChartView = view.findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(view.findViewById(R.id.avi));

        APIlib.getInstance().setActiveAnyChartView(anyChartView);


        diaTv = view.findViewById(R.id.diaTv);
        dplcTv = view.findViewById(R.id.dplcTv);
        priTv = view.findViewById(R.id.priTv);
        ftthTv = view.findViewById(R.id.ftthTv);
        iplcTv = view.findViewById(R.id.iplcTv);
        ipsecTv = view.findViewById(R.id.ipsecTv);
        m2mTv = view.findViewById(R.id.m2mTv);
        m2mATv = view.findViewById(R.id.m2mATv);
        sip_priTv = view.findViewById(R.id.sipPriTv);
        totalTv = view.findViewById(R.id.totalTv);

        diaTextTv = view.findViewById(R.id.diaTextTv);
        dplcTextTv = view.findViewById(R.id.dplcTextTv);
        priTextTv = view.findViewById(R.id.priTextTv);
        ftthTextTv = view.findViewById(R.id.ftthTextTv);
        iplcTextTv = view.findViewById(R.id.iplcTextTv);
        ipsecTextTv = view.findViewById(R.id.ipsecTextTv);
        m2mTextTv = view.findViewById(R.id.m2mTextTv);
        m2mATextTv = view.findViewById(R.id.m2mATextTv);
        sipPriTextTv = view.findViewById(R.id.sipPriTextTv);
        totalTextTv = view.findViewById(R.id.totalTextTv);

        diaTv.setOnClickListener(this);
        dplcTv.setOnClickListener(this);
        priTv.setOnClickListener(this);
        ftthTv.setOnClickListener(this);
        iplcTv.setOnClickListener(this);
        ipsecTv.setOnClickListener(this);
        m2mTv.setOnClickListener(this);
        m2mATv.setOnClickListener(this);
        sip_priTv.setOnClickListener(this);
        totalTv.setOnClickListener(this);

        diaTextTv.setOnClickListener(this);
        dplcTextTv.setOnClickListener(this);
        priTextTv.setOnClickListener(this);
        ftthTextTv.setOnClickListener(this);
        iplcTextTv.setOnClickListener(this);
        ipsecTextTv.setOnClickListener(this);
        m2mTextTv.setOnClickListener(this);
        m2mATextTv.setOnClickListener(this);
        sipPriTextTv.setOnClickListener(this);
        totalTextTv.setOnClickListener(this);

        getAllDatafromTicketsTable();

        populateRegion();
        setAnyChartView();

    }

    void populateRegion() {
        final List<String> regions = new ArrayList<>();
        regions.add(ALL_REGIONS);
        regions.add(NORTH);
        regions.add(CENTRAL);
        regions.add(SOUTH);
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, regions);
        regionSpinner.setAdapter(adap);
        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                regionName = regions.get(position);
                if (position == 0) {
                    if (ticketsList.size() != 0) {
                        serachFromList(ticketsList, regionName);
                    }

                } else if (position == 1) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(NORTH)) {
                            list.add(ticketsList.get(i));
                        }
                    }
//                    serachFromList(list, regionName);
                } else if (position == 2) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(CENTRAL)) {
                            list.add(ticketsList.get(i));
                        }
                    }
//                    serachFromList(list, regionName);
                } else if (position == 3) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(SOUTH)) {
                            list.add(ticketsList.get(i));
                        }
                    }
//                    serachFromList(list, regionName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setAnyChartView() {
        pie = AnyChart.pie();

        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry(Constants.DIA, diaCount));
        data.add(new ValueDataEntry(Constants.DPLC, dplcCount));
        data.add(new ValueDataEntry(Constants.PRI, priCount));
        data.add(new ValueDataEntry(Constants.FTTH, ftthCount));
        data.add(new ValueDataEntry(Constants.IPLC, iplcCount));
        data.add(new ValueDataEntry(Constants.IPSEC, ipsecCount));
        data.add(new ValueDataEntry(Constants.M2M, m2mCount));
        data.add(new ValueDataEntry(Constants.M2M_A, m2mACount));
        data.add(new ValueDataEntry(Constants.SIP_PRI, sipPriCount));

        pie.data(data);
//        pie.title("Status");
//        pie.labels().position("outside");
//        pie.legend()
//                .position("center-bottom")
//                .itemsLayout(LegendLayout.HORIZONTAL)
//                .align(Align.CENTER);
        String[] colors = new String[]{"#a93226", "#C0BBED", "#2471a3", "#17a589", "#00FF00", "#ecf0f1", "#f1c40f", "#800080", "#FF00FF"};
        pie.palette(colors);

        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
            @Override
            public void onClick(Event event) {

                Log.d("clickedName", event.getData().get("x"));

                switch (event.getData().get("x")) {
                    case Constants.DIA:
                        navigateToHistoryFragment("", "", Constants.DIA, "");
                        break;
                    case Constants.DPLC:
                        navigateToHistoryFragment("", "", Constants.DPLC, "");
                        break;
                    case Constants.PRI:
                        navigateToHistoryFragment("", "", Constants.PRI, "");
                        break;
                    case Constants.FTTH:
                        navigateToHistoryFragment("", "", Constants.FTTH, "");
                        break;
                    case Constants.IPLC:
                        navigateToHistoryFragment("", "", Constants.IPLC, "");
                        break;
                    case Constants.IPSEC:
                        navigateToHistoryFragment("", "", Constants.IPSEC, "");
                        break;
                    case Constants.M2M:
                        navigateToHistoryFragment("", "", Constants.M2M, "");
                        break;
                    case Constants.M2M_A:
                        navigateToHistoryFragment("", "", Constants.M2M_A, "");
                        break;
                    case Constants.SIP_PRI:
                        navigateToHistoryFragment("", "", Constants.SIP_PRI, "");
                        break;
                }
            }
        });
        anyChartView.invalidate();
        anyChartView.setChart(pie);

//        getAllDatafromTicketsTable();

    }

    void getAllDatafromTicketsTable() {
        startAnim();

        openList = new ArrayList<>();
        closedList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TICKET_TABLE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                diaCount = 0;
                dplcCount = 0;
                priCount = 0;
                iplcCount = 0;
                ftthCount = 0;
                ipsecCount = 0;
                m2mCount = 0;
                m2mACount = 0;
                sipPriCount = 0;

                totalCount = 0;
                stopAnim();
                ticketsList = new ArrayList<>();

                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataValues : dataSnapshot.getChildren()) {
                    TTModel ttModel = dataValues.getValue(TTModel.class);
                    ticketsList.add(ttModel);
                }
                serachFromList(ticketsList, "All Regions");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void serachFromList(List<TTModel> list, String title) {

        diaCount = 0;
        dplcCount = 0;
        priCount = 0;
        iplcCount = 0;
        ftthCount = 0;
        ipsecCount = 0;
        m2mCount = 0;
        m2mACount = 0;
        sipPriCount = 0;

        totalCount = 0;

        openList = new ArrayList<>();
        closedList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getServiceType().contains(Constants.DIA)) {
                diaCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().contains(Constants.DPLC)) {
                dplcCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().equals(Constants.PRI)) {
                priCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().contains(Constants.FTTH)) {
                ftthCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().contains(Constants.IPLC)) {
                iplcCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().contains(Constants.IPSEC)) {
                ipsecCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().equals(Constants.M2M)) {
                m2mCount++;
                totalCount++;
            } else if (list.get(i).getServiceType().equals(Constants.M2M_A)) {
                m2mACount++;
                totalCount++;
            } else if (list.get(i).getServiceType().equals(Constants.SIP_PRI)) {
                sipPriCount++;
                totalCount++;
            }
        }

        setCharValues(diaCount, dplcCount, priCount, ftthCount, iplcCount, ipsecCount, m2mCount, m2mACount, sipPriCount, totalCount, title);
    }

    void setCharValues(final int diaCount, final int dplcCount, int priCount, int ftthCount,
                       int iplcCount, int ipsecCount, int m2mCount, int m2mACount, int sipPriCount, int totalCount, String title) {

        diaTv.setText(diaCount + "");
        dplcTv.setText(dplcCount + "");
        priTv.setText(priCount + "");
        ftthTv.setText(ftthCount + "");
        iplcTv.setText(iplcCount + "");
        ipsecTv.setText(ipsecCount + "");
        m2mTv.setText(m2mCount + "");
        m2mATv.setText(m2mACount + "");
        sip_priTv.setText(sipPriCount + "");
        totalTv.setText(totalCount + "");

        List<DataEntry> data = new ArrayList<>();

        data.add(new ValueDataEntry(Constants.DIA, diaCount));
        data.add(new ValueDataEntry(Constants.DPLC, dplcCount));
        data.add(new ValueDataEntry(Constants.PRI, priCount));
        data.add(new ValueDataEntry(Constants.FTTH, ftthCount));
        data.add(new ValueDataEntry(Constants.IPLC, iplcCount));
        data.add(new ValueDataEntry(Constants.IPSEC, ipsecCount));
        data.add(new ValueDataEntry(Constants.M2M, m2mCount));
        data.add(new ValueDataEntry(Constants.M2M_A, m2mACount));
        data.add(new ValueDataEntry(Constants.SIP_PRI, sipPriCount));

        pie.data(data);
        pie.title(title);

        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
            @Override
            public void onClick(Event event) {

                Log.d("clickedName", event.getData().get("x"));

                switch (event.getData().get("x")) {
                    case Constants.DIA:
                        navigateToHistoryFragment("", "", Constants.DIA, "");
                        break;
                    case Constants.DPLC:
                        navigateToHistoryFragment("", "", Constants.DPLC, "");
                        break;
                    case Constants.PRI:
                        navigateToHistoryFragment("", "", Constants.PRI, "");
                        break;
                    case Constants.FTTH:
                        navigateToHistoryFragment("", "", Constants.FTTH, "");
                        break;
                    case Constants.IPLC:
                        navigateToHistoryFragment("", "", Constants.IPLC, "");
                        break;
                    case Constants.IPSEC:
                        navigateToHistoryFragment("", "", Constants.IPSEC, "");
                        break;
                    case Constants.M2M:
                        navigateToHistoryFragment("", "", Constants.M2M, "");
                        break;
                    case Constants.M2M_A:
                        navigateToHistoryFragment("", "", Constants.M2M_A, "");
                        break;
                    case Constants.SIP_PRI:
                        navigateToHistoryFragment("", "", Constants.SIP_PRI, "");
                        break;
                }
            }
        });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        if (avi != null) {
            avi.hide();
            avi.setVisibility(View.GONE);
        }
        // or avi.smoothToHide();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        stopAnim();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.diaTextTv:
                navigateToHistoryFragment("", "", Constants.DIA, "");
                break;
            case R.id.diaTv:
                navigateToHistoryFragment("", "", Constants.DIA, "");
                break;
            case R.id.dplcTextTv:
                navigateToHistoryFragment("", "", Constants.DPLC, "");
                break;
            case R.id.dplcTv:
                navigateToHistoryFragment("", "", Constants.DPLC, "");
                break;
            case R.id.priTextTv:
                navigateToHistoryFragment("", "", Constants.PRI, "");
                break;
            case R.id.priTv:
                navigateToHistoryFragment("", "", Constants.PRI, "");
                break;
            case R.id.ftthTextTv:
                navigateToHistoryFragment("", "", Constants.FTTH, "");
                break;
            case R.id.ftthTv:
                navigateToHistoryFragment("", "", Constants.FTTH, "");
                break;
            case R.id.iplcTextTv:
                navigateToHistoryFragment("", "", Constants.IPLC, "");
                break;
            case R.id.iplcTv:
                navigateToHistoryFragment("", "", Constants.IPLC, "");
                break;
            case R.id.ipsecTextTv:
                navigateToHistoryFragment("", "", Constants.IPSEC, "");
                break;
            case R.id.ipsecTv:
                navigateToHistoryFragment("", "", Constants.IPSEC, "");
                break;
            case R.id.m2mATv:
                navigateToHistoryFragment("", "", Constants.M2M_A, "");
                break;
            case R.id.m2mATextTv:
                navigateToHistoryFragment("", "", Constants.M2M_A, "");
                break;
            case R.id.m2mTv:
                navigateToHistoryFragment("", "", Constants.M2M, "");
                break;
            case R.id.m2mTextTv:
                break;
            case R.id.sipPriTextTv:
                navigateToHistoryFragment("", "", Constants.SIP_PRI, "");
                break;
            case R.id.sipPriTv:
                navigateToHistoryFragment("", "", Constants.SIP_PRI, "");
                break;

        }
    }

    void navigateToHistoryFragment(String region, String status, String serviceType, String name) {

        Bundle bundle = new Bundle();
        bundle.putString("Type", region);
        bundle.putString("Status", status);
        bundle.putString("name", name);
        bundle.putString("direction", Constants.FragmentSide);
        bundle.putString("serviceType", serviceType);

        HistoryFragment historyFragment = new HistoryFragment();
        historyFragment.setArguments(bundle);

        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, historyFragment);
//        transaction.addToBackStack(null);
        transaction.commit();

    }

//        DrawerActivity.fragmentName.setText(name);

}