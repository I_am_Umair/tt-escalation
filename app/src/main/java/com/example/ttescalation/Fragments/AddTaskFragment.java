package com.example.ttescalation.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ttescalation.Model.CommentModel;
import com.example.ttescalation.Model.TaskModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddTaskFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddTaskFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddTaskFragment newInstance(String param1, String param2) {
        AddTaskFragment fragment = new AddTaskFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Spinner statusSpinner;

    EditText taskNameEt , taskDetailsEt , actionOwnerEt;
    Button addBtn;
    private DatabaseReference mDatabase;
    AVLoadingIndicatorView avi;
    String statusName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_task, container, false);

        setxml(view);

        return view;
    }

    void setxml (View view){
        addBtn = view.findViewById(R.id.addBtn);
        taskNameEt = view.findViewById(R.id.taskNameEt);
        taskDetailsEt = view.findViewById(R.id.taskDetailsEt);
        statusSpinner = view.findViewById(R.id.statusSpinner);
        actionOwnerEt = view.findViewById(R.id.actionOwnerEt);
        avi = view.findViewById(R.id.avi);

        addBtn.setOnClickListener(this);

        populateStatus();
        focusedInputLayouts();
    }

    void populateStatus(){
        final List<String> statuses = new ArrayList<>();
        statuses.add(Constants.OPEN);
        statuses.add(Constants.CLOSED);

        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, statuses);
        statusSpinner.setAdapter(adap);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusName = statuses.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addBtn:
                String name = taskNameEt.getText().toString().trim();
                String details = taskDetailsEt.getText().toString().trim();
                String currentStatus = statusName.trim();
                String owner = actionOwnerEt.getText().toString().trim();

                if (name.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter Task name !", Toast.LENGTH_LONG).show();
                }else if (details.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter some Task details !", Toast.LENGTH_LONG).show();
                }else if (currentStatus.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter current status of task !", Toast.LENGTH_LONG).show();
                }else if (owner.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please mention Action owner for this task !", Toast.LENGTH_LONG).show();
                }else {
                    addTask(name , details , currentStatus , owner);
                }

                break;
        }
    }

    void addTask (String name , String details , String status , String owner){
        startAnim();
        String currentDate = "";
        Date date = null;
        try {
            currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            date = (Date)formatter.parse(currentDate);
            System.out.println("Today_is " +date.getTime());
        }catch (Exception e){ }


        List<CommentModel> comments = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final String key = mDatabase.push().getKey();
        TaskModel taskModel = new TaskModel(key  , name , details , status , owner , date.getTime() , 0 , comments);

        mDatabase.child(Constants.TASK_TABLE).child(key).setValue(taskModel).
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        stopAnim();
                        if (task.isSuccessful()) {
                            Toast.makeText(SystemUtils.getActivity(), "Task created Successfully !", Toast.LENGTH_LONG).show();
                            taskNameEt.getText().clear();
                            taskDetailsEt.getText().clear();
                            actionOwnerEt.getText().clear();

//                            navigateToHistoryFragment(Constants.ALL_REGIONS, "null");
                        } else if (task.isCanceled())
                            Toast.makeText(SystemUtils.getActivity(), "Some error occurred ", Toast.LENGTH_LONG).show();
                    }
                });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    void focusedInputLayouts (){
        taskNameEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    taskNameEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    taskNameEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
        taskDetailsEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    taskDetailsEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    taskDetailsEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });

        actionOwnerEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    actionOwnerEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    actionOwnerEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });

    }
}