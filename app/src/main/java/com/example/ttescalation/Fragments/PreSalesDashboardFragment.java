package com.example.ttescalation.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.TaskModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PreSalesDashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreSalesDashboardFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PreSalesDashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PreSalesDashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PreSalesDashboardFragment newInstance(String param1, String param2) {
        PreSalesDashboardFragment fragment = new PreSalesDashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    AnyChartView anyChartView;
    Pie pie;

    int openCount = 0, closedCount = 0, totalCount = 0;
    AVLoadingIndicatorView avi;
    private DatabaseReference mDatabase;
    List<TaskModel> ticketsList = new ArrayList<>();
    TextView totalTv, openTv, closedTv, closedTextTv, openTextTv, totalTextTv;
    UserModel userModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pre_sales_dashboard, container, false);

        setXml(view);
        return view;
    }

    void setXml(View view) {
        anyChartView = view.findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(view.findViewById(R.id.avi));
        avi = view.findViewById(R.id.avi);

        APIlib.getInstance().setActiveAnyChartView(anyChartView);

        openTv = view.findViewById(R.id.openTv);
        openTextTv = view.findViewById(R.id.openTextTv);
        closedTv = view.findViewById(R.id.closedTv);
        closedTextTv = view.findViewById(R.id.closedTextTv);
        totalTv = view.findViewById(R.id.totalTv);
        totalTextTv = view.findViewById(R.id.totalTextTv);

        totalTv.setOnClickListener(this);
        totalTextTv.setOnClickListener(this);

        closedTv.setOnClickListener(this);
        closedTextTv.setOnClickListener(this);

        openTv.setOnClickListener(this);
        openTextTv.setOnClickListener(this);

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);

        setAnyChartView();
    }

    void setAnyChartView() {
        pie = AnyChart.pie();

        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry(Constants.OPEN, openCount));
        data.add(new ValueDataEntry(Constants.CLOSED, closedCount));
        pie.data(data);

        pie.title("Tasks");
        pie.labels().position("outside");
        pie.legend()
                .position("center-bottom")
                .itemsLayout(LegendLayout.HORIZONTAL)
                .align(Align.CENTER);
        String[] colors = new String[]{"#F69090", "#aed581", "#FACC3D", "#e6ee9c", "#ffcc80"};
        pie.palette(colors);

        anyChartView.invalidate();
        anyChartView.setChart(pie);


        getAllDataFromTasksTable();
    }

    void getAllDataFromTasksTable() {
        startAnim();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TASK_TABLE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                totalCount = 0;
                stopAnim();
                ticketsList = new ArrayList<>();

                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataValues : dataSnapshot.getChildren()) {
                    TaskModel ttModel = dataValues.getValue(TaskModel.class);
                    ticketsList.add(ttModel);
                }
                serachFromList(ticketsList, "All Regions");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void serachFromList(List<TaskModel> list, String title) {

        openCount = 0;
        closedCount = 0;
        totalCount = 0;

//        openList = new ArrayList<>();
//        closedList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getStatus().equals(Constants.OPEN)) {
                openCount++;
//                openList.add(list.get(i));
                totalCount++;

            } else if (list.get(i).getStatus().equals(Constants.CLOSED)) {
                closedCount++;
//                closedList.add(list.get(i));
                totalCount++;

            }
        }

        setCharValues(openCount, closedCount, totalCount, title);
    }

    void setCharValues(final int openCount, final int closedCount, int totalCount, String title) {

        openTv.setText(openCount + "");
        closedTv.setText(closedCount + "");
        totalTv.setText(totalCount + "");

        List<DataEntry> data = new ArrayList<>();

        data.add(new ValueDataEntry(Constants.OPEN, openCount));
        data.add(new ValueDataEntry(Constants.CLOSED, closedCount));

        pie.data(data);
        pie.title(title);

        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
            @Override
            public void onClick(Event event) {

                Log.d("clickedName", event.getData().get("x"));

                if (event.getData().get("x").equals(Constants.OPEN)) {
                    navigateToHistoryFragment(Constants.OPEN, "Open Tickets");
                } else if (event.getData().get("x").equals(Constants.CLOSED)) {
                    navigateToHistoryFragment(Constants.CLOSED, "Closed Tickets");
                }
            }
        });
    }

    void navigateToHistoryFragment(String status, String name) {

        if (status.equals(Constants.CLOSED)) {
            if (userModel.isAdmin()) {
                Bundle bundle = new Bundle();
                bundle.putString("Status", status);

                PreSalesHistory historyFragment = new PreSalesHistory();
                historyFragment.setArguments(bundle);

                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, historyFragment);
                transaction.commit();
            } else {
                Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see closed tasks", Toast.LENGTH_LONG).show();
            }
        } else if (status.equals(Constants.OPEN)) {
            Bundle bundle = new Bundle();
            bundle.putString("Status", status);

            PreSalesHistory historyFragment = new PreSalesHistory();
            historyFragment.setArguments(bundle);

            FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, historyFragment);
            transaction.commit();
        }

//        DrawerActivity.fragmentName.setText(name);

    }


    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        if (avi != null) {
            avi.hide();
            avi.setVisibility(View.GONE);
        }
        // or avi.smoothToHide();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        stopAnim();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.openTextTv:
                navigateToHistoryFragment( Constants.OPEN, "Open Tickets");
                break;
            case R.id.openTv:
                navigateToHistoryFragment( Constants.OPEN, "Open Tickets");
                break;
            case R.id.closedTextTv:
                navigateToHistoryFragment(Constants.CLOSED, "Closed Tickets");
                break;
            case R.id.closedTv:
                navigateToHistoryFragment(Constants.CLOSED, "Closed Tickets");
                break;
            case R.id.totalTextTv:
                if (userModel.isAdmin()) {
//                    DrawerActivity.fragmentName.setText("Tickets History");
                    Bundle bundle = new Bundle();
                    bundle.putString("Status", "Total");

                    PreSalesHistory historyFragment = new PreSalesHistory();
                    historyFragment.setArguments(bundle);

                    FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment, historyFragment);
//                    transaction.addToBackStack(null);
                    transaction.commit();
                } else
                    Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see All tasks", Toast.LENGTH_LONG).show();
                break;
            case R.id.totalTv:
                if (userModel.isAdmin()) {
//                    DrawerActivity.fragmentName.setText("Tickets History");
                    Bundle bundle = new Bundle();
                    bundle.putString("Status", "Total");

                    PreSalesHistory historyFragment = new PreSalesHistory();
                    historyFragment.setArguments(bundle);

                    FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment, historyFragment);
//                    transaction.addToBackStack(null);
                    transaction.commit();
                } else
                    Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see All tickets", Toast.LENGTH_LONG).show();
                break;
        }

    }
}