package com.example.ttescalation.Fragments;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Cartesian3d;
import com.anychart.charts.Pie;
import com.anychart.data.Set;
import com.anychart.enums.Align;
import com.anychart.enums.HoverMode;
import com.anychart.enums.LegendLayout;
import com.anychart.enums.TooltipDisplayMode;
import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.SendNotificationPack.Token;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener {

    public DashboardFragment() {
        // Required empty public constructor
    }

    AVLoadingIndicatorView avi;
    Spinner regionSpinner;
    AnyChartView anyChartView;
    Pie pie;


    String regionName = "";

    int northCount = 0;
    int centralCount = 0;
    int southCount = 0;
    int totalCount = 0;

    int openCount = 0;
    int closedCount = 0;

    private static final String NORTH = "North";
    private static final String CENTRAL = "Central";
    private static final String SOUTH = "South";
    private static final String ALL_REGIONS = "All Regions";
    private static final String OPEN = "Open";
    private static final String CLOSED = "Closed";

    private DatabaseReference mDatabase;
    List<TTModel> ticketsList = new ArrayList<>();
    List<TTModel> openList = new ArrayList<>();
    List<TTModel> closedList = new ArrayList<>();

    TextView totalTv, openTv, closedTv, closedTextTv, openTextTv, totalTextTv;

    UserModel userModel;
    FirebaseUser fuser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        setXml(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    void setXml(View view) {
        avi = view.findViewById(R.id.avi);
        regionSpinner = view.findViewById(R.id.regionSpinner);
        anyChartView = view.findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(view.findViewById(R.id.avi));

        APIlib.getInstance().setActiveAnyChartView(anyChartView);

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);

        openTv = view.findViewById(R.id.openTv);
        openTextTv = view.findViewById(R.id.openTextTv);
        closedTv = view.findViewById(R.id.closedTv);
        closedTextTv = view.findViewById(R.id.closedTextTv);
        totalTv = view.findViewById(R.id.totalTv);

        totalTextTv = view.findViewById(R.id.totalTextTv);
        totalTv.setOnClickListener(this);

        closedTv.setOnClickListener(this);
        closedTextTv.setOnClickListener(this);

        openTv.setOnClickListener(this);
        openTextTv.setOnClickListener(this);

        populateRegion();
        setAnyChartView();

//        firebaseNotiToken();
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        updateToken(FirebaseInstanceId.getInstance().getToken() );

    }

    void populateRegion() {
        final List<String> regions = new ArrayList<>();
        regions.add(ALL_REGIONS);
        regions.add(NORTH);
        regions.add(CENTRAL);
        regions.add(SOUTH);
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, regions);
        regionSpinner.setAdapter(adap);
        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                regionName = regions.get(position);
                if (position == 0) {
                    if (ticketsList.size() != 0)
                        serachFromList(ticketsList, regionName);

                } else if (position == 1) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(NORTH)) {
                            list.add(ticketsList.get(i));
                        }
                    }
                    serachFromList(list, regionName);
                } else if (position == 2) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(CENTRAL)) {
                            list.add(ticketsList.get(i));
                        }
                    }
                    serachFromList(list, regionName);
                } else if (position == 3) {
                    List<TTModel> list = new ArrayList<>();
                    for (int i = 0; i < ticketsList.size(); i++) {
                        if (ticketsList.get(i).getRegion().equals(SOUTH)) {
                            list.add(ticketsList.get(i));
                        }
                    }
                    serachFromList(list, regionName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setAnyChartView() {
        pie = AnyChart.pie();

        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("Open", 0));
        data.add(new ValueDataEntry("Closed", 0));

        pie.data(data);
        pie.title("Status");
        pie.labels().position("outside");
        pie.legend()
                .position("center-bottom")
                .itemsLayout(LegendLayout.HORIZONTAL)
                .align(Align.CENTER);
        String[] colors = new String[]{"#F69090", "#aed581", "#FACC3D", "#e6ee9c", "#ffcc80"};
        pie.palette(colors);

        anyChartView.invalidate();
        anyChartView.setChart(pie);

        getAllDatafromTicketsTable();

    }

    void getAllDatafromTicketsTable() {
        startAnim();

        openList = new ArrayList<>();
        closedList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -30);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TICKET_TABLE).orderByChild("timeStamp")
                .startAt(calendar.getTimeInMillis())
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                northCount = 0;
                centralCount = 0;
                southCount = 0;
                totalCount = 0;
                stopAnim();
                ticketsList = new ArrayList<>();

                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataValues : dataSnapshot.getChildren()){
                    TTModel ttModel = dataValues.getValue(TTModel.class);
                    ticketsList.add(ttModel);
                }
                serachFromList(ticketsList, "All Regions");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        if (avi != null) {
            avi.hide();
            avi.setVisibility(View.GONE);
        }
        // or avi.smoothToHide();
    }

    @Override
    public void onResume() {
        super.onResume();
        DrawerActivity.fragmentName.setText("Dashboard");
        DrawerActivity.logoutIv.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        DrawerActivity.logoutIv.setVisibility(View.GONE);
        stopAnim();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        stopAnim();
        super.onDestroyView();
    }

    void serachFromList(List<TTModel> list, String title) {

        openCount = 0;
        closedCount = 0;
        totalCount = 0;

        openList = new ArrayList<>();
        closedList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getStatus().equals(OPEN)) {
                openCount++;
                openList.add(list.get(i));
                totalCount++;

            } else if (list.get(i).getStatus().equals(CLOSED)) {
                closedCount++;
                closedList.add(list.get(i));
                totalCount++;

            }
        }

        setCharValues(openCount, closedCount, totalCount, title);
    }

    void setCharValues(final int openCount, final int closedCount, int totalCount, String title) {

        openTv.setText(openCount + "");
        closedTv.setText(closedCount + "");
        totalTv.setText(totalCount + "");

        List<DataEntry> data = new ArrayList<>();

        data.add(new ValueDataEntry(OPEN, openCount));
        data.add(new ValueDataEntry(CLOSED, closedCount));

        pie.data(data);
        pie.title(title);

        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
            @Override
            public void onClick(Event event) {

                Log.d("clickedName", event.getData().get("x"));

                if (event.getData().get("x").equals(Constants.OPEN)) {
                    navigateToHistoryFragment(regionName, Constants.OPEN, "Open Tickets");
                } else if (event.getData().get("x").equals(Constants.CLOSED)) {
                    navigateToHistoryFragment(regionName, Constants.CLOSED, "Closed Tickets");
                }
            }
        });
    }

    void navigateToHistoryFragment(String region, String status, String name) {

        if (status.equals(Constants.CLOSED)) {
            if (userModel.isAdmin()) {
                Bundle bundle = new Bundle();
                bundle.putString("Type", region);
                bundle.putString("Status", status);
                bundle.putString("name", name);
                bundle.putString("direction", Constants.DashboardSide);

                HistoryFragment historyFragment = new HistoryFragment();
                historyFragment.setArguments(bundle);

                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, historyFragment , "HistoryFragment");
                transaction.commit();
            } else {
                Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see closed tickets", Toast.LENGTH_LONG).show();
            }
        } else if (status.equals(Constants.OPEN)) {
            Bundle bundle = new Bundle();
            bundle.putString("Type", region);
            bundle.putString("Status", status);
            bundle.putString("name", name);
            bundle.putString("direction", Constants.DashboardSide);

            HistoryFragment historyFragment = new HistoryFragment();
            historyFragment.setArguments(bundle);

            FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, historyFragment , "HistoryFragment");
            transaction.commit();
        }

//        DrawerActivity.fragmentName.setText(name);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.openTextTv:
                navigateToHistoryFragment(regionName, Constants.OPEN, "Open Tickets");
                break;
            case R.id.openTv:
                navigateToHistoryFragment(regionName, Constants.OPEN, "Open Tickets");
                break;
            case R.id.closedTextTv:
                navigateToHistoryFragment(regionName, Constants.CLOSED, "Closed Tickets");
                break;
            case R.id.closedTv:
                navigateToHistoryFragment(regionName, Constants.CLOSED, "Closed Tickets");
                break;
            case R.id.totalTextTv:
                if (userModel.isAdmin()) {
                    DrawerActivity.fragmentName.setText("Tickets History");
                    Bundle bundle = new Bundle();
                    bundle.putString("Type", Constants.ALL_REGIONS);
                    bundle.putString("Status", "null");
                    bundle.putString("direction", Constants.DashboardSide);

                    HistoryFragment historyFragment = new HistoryFragment();
                    historyFragment.setArguments(bundle);

                    FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment, historyFragment , "HistoryFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else
                    Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see All tickets", Toast.LENGTH_LONG).show();
                break;
            case R.id.totalTv:
                if (userModel.isAdmin()) {
                    DrawerActivity.fragmentName.setText("Tickets History");
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("Type", Constants.ALL_REGIONS);
                    bundle1.putString("Status", "null");
                    bundle1.putString("direction", Constants.DashboardSide);

                    HistoryFragment historyFragment1 = new HistoryFragment();
                    historyFragment1.setArguments(bundle1);

                    FragmentTransaction transaction1 = requireActivity().getSupportFragmentManager().beginTransaction();
                    transaction1.replace(R.id.nav_host_fragment, historyFragment1 , "HistoryFragment");
                    transaction1.addToBackStack(null);
                    transaction1.commit();
                    break;
                } else
                    Toast.makeText(SystemUtils.getActivity(), "Sorry ! You dont have rights to see All tickets", Toast.LENGTH_LONG).show();
        }
    }


    void firebaseNotiToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//To do//
                            return;
                        }

// Get the Instance ID token//
                        String token = task.getResult().getToken();
                        String msg = getString(R.string.fcm_token, token);
                        Log.d(TAG, msg);

                    }
                });
    }

    private void updateToken(String token) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token , userModel.getId());
        reference.child(userModel.getId()).setValue(token1);
    }
}
