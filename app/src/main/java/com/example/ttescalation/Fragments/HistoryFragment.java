package com.example.ttescalation.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Adapters.HistoryAdapter;
import com.example.ttescalation.Adapters.SwipeRecyclerView.RecyclerItemTouchShelf;
import com.example.ttescalation.Adapters.SwipeRecyclerView.SwipeHelper;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.RecyclerViewHandler;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static java.util.Calendar.MONTH;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements RecyclerItemTouchShelf.RecyclerItemTouchHelperListener, View.OnClickListener {

    public HistoryFragment() {
        // Required empty public constructor
    }

    RecyclerView recyclerView;
    List<TTModel> ticketsList = new ArrayList<>();
    HistoryAdapter adapter;
    private DatabaseReference mDatabase;
    AVLoadingIndicatorView avi;
    TextView noItemTv;
    ImageView crossIV;
    HistoryAdapter.RecyclerItemClickListener recyclerItemClickListener = null;
    EditText ticketDetailEt;

    Spinner regionSpinner, statusSpinner, typeSpinner, ownerSpinner;
    String regionName = "";
    String statusName = "";
    String serviceType = "";
    String ownerName = "";
    AutoCompleteTextView autoCompleteTextView;
    UserModel userModel;
    AutoCompleteTextView autoTextClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        avi = view.findViewById(R.id.avi);
        recyclerView = view.findViewById(R.id.recyclerView);
        noItemTv = view.findViewById(R.id.noItemTv);
        autoTextClient = view.findViewById(R.id.autoTextClient);
        crossIV = view.findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerView(ticketsList);
                autoTextClient.getText().clear();
            }
        });

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);
        recyclerView = new RecyclerViewHandler().settingRecyleView(recyclerView, LinearLayout.VERTICAL, SystemUtils.getActivity());
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchShelf(0, ItemTouchHelper.LEFT
                | ItemTouchHelper.RIGHT, this, SystemUtils.getActivity());
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        recyclerItemClickListener = new HistoryAdapter.RecyclerItemClickListener() {
            @Override
            public void onClick(int position, TTModel datum, boolean showDetail, boolean delete) {
//                if (showDetail)
//                    showDetailDialog(datum);
//                else
                if (userModel.isAdmin())
                    showEditDialog(datum);
                else
                    Toast.makeText(SystemUtils.getActivity(), "You don't have rights to edit ticket", Toast.LENGTH_LONG).show();
            }
        };
        Bundle bundle = this.getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            String type = bundle.getString("Type");
            String status = bundle.getString("Status");
            String direction = bundle.getString("direction");
            String serviceType = bundle.getString("serviceType");

            if (bundle.getString("name") != null)
                DrawerActivity.fragmentName.setText(bundle.getString("name"));


            Log.d("Arguments", type);
            if (direction == Constants.DashboardSide) {
                if (type.equals(Constants.ALL_REGIONS)) {
                    getAllDatafromTicketsTable(status);
                } else if (type.equals(Constants.NORTH)) {
                    getDataFromTicketTableByType(Constants.NORTH, status);
                } else if (type.equals(Constants.SOUTH)) {
                    getDataFromTicketTableByType(Constants.SOUTH, status);
                } else {
                    getDataFromTicketTableByType(Constants.CENTRAL, status);
                }
            }else if (direction == Constants.FragmentSide){
                getDataFromTicketTableByServiceType(serviceType );
            }
        }

        populateClients();

        autoTextClient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(SystemUtils.getActivity(), "Performing search ", Toast.LENGTH_SHORT).show();
                    performSearch(ticketsList, autoTextClient.getText().toString().toLowerCase());
                    return true;
                }
                return false;
            }
        });

        return view;
    }


    void setRecyclerView(List<TTModel> list) {
        Collections.reverse(list);
        if (list.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new HistoryAdapter(list, SystemUtils.getActivity(), recyclerItemClickListener);
            recyclerView.setAdapter(adapter);
            noItemTv.setVisibility(View.GONE);

        } else {
            recyclerView.setVisibility(View.GONE);
            noItemTv.setVisibility(View.VISIBLE);
        }
    }

    void getAllDatafromTicketsTable(final String status) {
        startAnim();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        String today = sdf.format(Calendar.getInstance().getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        String oneMonthAgo = sdf.format(calendar.getTimeInMillis());

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TICKET_TABLE).orderByChild("timeStamp")
                .startAt(calendar.getTimeInMillis())
//                .endAt(today)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ticketsList = new ArrayList<>();
                stopAnim();
                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TTModel listdata = dataSnapshot1.getValue(TTModel.class);
                    if (status != "null") {
                        if (listdata.getStatus().equals(status)) {
                            ticketsList.add(listdata);
                        }
                    } else {
                        ticketsList.add(listdata);
                    }
                }
                setRecyclerView(ticketsList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void getDataFromTicketTableByType(String region, final String status) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TICKET_TABLE)
                .orderByChild("region").equalTo(region).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ticketsList = new ArrayList<>();

                stopAnim();
                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TTModel listdata = dataSnapshot1.getValue(TTModel.class);
                    if (listdata.getStatus().equals(status))
                        ticketsList.add(listdata);
                }
                setRecyclerView(ticketsList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void getDataFromTicketTableByServiceType(String serviceType) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TICKET_TABLE)
                .orderByChild("serviceType").equalTo(serviceType).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ticketsList = new ArrayList<>();

                stopAnim();
                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TTModel listdata = dataSnapshot1.getValue(TTModel.class);
//                    if (listdata.getStatus().equals(status))
                        ticketsList.add(listdata);
                }
                setRecyclerView(ticketsList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    void showDetailDialog(TTModel ttModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.detail_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        EditText ticketIDEt = dialog.findViewById(R.id.ticketIDEt);
        EditText ticketCreatorEt = dialog.findViewById(R.id.ticketCreatorEt);
        EditText dateTimeEt = dialog.findViewById(R.id.dateTimeEt);
        EditText ticketDetailEt = dialog.findViewById(R.id.ticketDetailEt);
        EditText clientEt = dialog.findViewById(R.id.clientEt);
        EditText ticketTypeET = dialog.findViewById(R.id.ticketTypeET);
        EditText ticketRegionEt = dialog.findViewById(R.id.ticketRegionEt);
        EditText ticketStatusEt = dialog.findViewById(R.id.ticketStatusEt);
        EditText ettrEt = dialog.findViewById(R.id.ettrEt);

        ticketIDEt.setText(ttModel.getId());
        ticketCreatorEt.setText(ttModel.getCreator());
        dateTimeEt.setText(getDate(ttModel.getTimeStamp()));
        ticketDetailEt.setText(ttModel.getTicketDetails());
        clientEt.setText(ttModel.getClientName());
        ticketTypeET.setText(ttModel.getServiceType());
        ticketRegionEt.setText(ttModel.getRegion());
        ticketStatusEt.setText(ttModel.getStatus());
        ettrEt.setText(ttModel.getEttr());

        dialog.show();

    }

    private String getDate(long time_stamp_server) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatter.format(time_stamp_server);
    }

    void showEditDialog(final TTModel ttModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.edit_ticket_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ticketDetailEt = dialog.findViewById(R.id.ticketDetailEt);
        final EditText ticketEttrEt = dialog.findViewById(R.id.ticketEttrEt);
        regionSpinner = dialog.findViewById(R.id.regionSpinner);
        statusSpinner = dialog.findViewById(R.id.statusSpinner);
        typeSpinner = dialog.findViewById(R.id.typeSpinner);
        ownerSpinner = dialog.findViewById(R.id.ownerSpinner);
        autoCompleteTextView = dialog.findViewById(R.id.autoTextView);
        final TextView ticketTypeTv = dialog.findViewById(R.id.ticketTypeTv);
        final TextView regionTv = dialog.findViewById(R.id.regionTv);
        final TextView statusTv = dialog.findViewById(R.id.statusTv);

        Button editBtn = dialog.findViewById(R.id.editBtn);

        final List<String> serviceTypes = new ArrayList<>();
        serviceTypes.add("DIA");
        serviceTypes.add("DPLC");
        serviceTypes.add("PRI");
        serviceTypes.add("FTTH");
        serviceTypes.add("IPLC");
        serviceTypes.add("IPSec");
        serviceTypes.add("M2M");
        serviceTypes.add("M2M (Aggregation Down)");
        serviceTypes.add("SIP PRI");
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, serviceTypes);
        typeSpinner.setAdapter(adap);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceType = serviceTypes.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switch (ttModel.getServiceType()) {
            case "DIA":
                typeSpinner.setSelection(0);
                break;
            case "DPLC":
                typeSpinner.setSelection(1);
                break;
            case "PRI":
                typeSpinner.setSelection(2);
                break;
            case "FTTH":
                typeSpinner.setSelection(3);
                break;
            case "IPLC":
                typeSpinner.setSelection(4);
                break;
            case "IPSec":
                typeSpinner.setSelection(5);
                break;
            case "M2M":
                typeSpinner.setSelection(6);
                break;
            case "M2M (Aggregation Down)":
                typeSpinner.setSelection(7);
                break;
            case "SIP PRI":
                typeSpinner.setSelection(8);
                break;
            default:
                typeSpinner.setSelection(0);
                break;
        }

        final List<String> regions = new ArrayList<>();
        regions.add("North");
        regions.add("Central");
        regions.add("South");
        SpinnerAdapter adapt = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, regions);
        regionSpinner.setAdapter(adapt);
        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                regionName = regions.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (ttModel.getRegion() != null) {
            switch (ttModel.getRegion()) {
                case "North":
                    regionSpinner.setSelection(0);
                    break;
                case "Central":
                    regionSpinner.setSelection(1);
                    break;
                case "South":
                    regionSpinner.setSelection(2);
                    break;
                default:
                    regionSpinner.setSelection(0);
                    break;
            }
        }
        final List<String> statuses = new ArrayList<>();
        statuses.add(Constants.OPEN);
        statuses.add(Constants.CLOSED);
        SpinnerAdapter adapte = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, statuses);
        statusSpinner.setAdapter(adapte);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusName = statuses.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (ttModel.getStatus() != null) {
            switch (ttModel.getStatus()) {
                case Constants.OPEN:
                    statusSpinner.setSelection(0);
                    break;
                case Constants.CLOSED:
                    statusSpinner.setSelection(1);
                    break;
                default:
                    statusSpinner.setSelection(0);
                    break;
            }
        }
        String[] fruits = {"HEC", "Farm Fresh Foods", "Bank Alhabib", "Traditions", "HBL",
                "Mari Petroleum", "CPHGC", "Bestway Cement", "HBL", "NHA", "MCB"};

        ArrayAdapter<String> adaptr = new ArrayAdapter<String>(SystemUtils.getActivity(),
                R.layout.spinner_item, fruits);
        autoCompleteTextView.setThreshold(1); //will start working from first character
        autoCompleteTextView.setAdapter(adaptr);

        final String[] owners = {Constants.VENDOR, Constants.NOMC, Constants.CUSTOMER};

        SpinnerAdapter ownerAdapter = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, owners);
        ownerSpinner.setAdapter(ownerAdapter);

        ownerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ownerName = owners[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (ttModel.getActionOwner() != null) {
            switch (ttModel.getActionOwner()) {
                case Constants.VENDOR:
                    ownerSpinner.setSelection(0);
                    break;
                case Constants.NOMC:
                    ownerSpinner.setSelection(1);
                    break;
                case Constants.CUSTOMER:
                    ownerSpinner.setSelection(2);
                    break;
            }
        }
        focusedInputLayouts();

        ticketDetailEt.setText(ttModel.getTicketDetails());
        ticketEttrEt.setText(ttModel.getEttr());
        autoCompleteTextView.setText(ttModel.getClientName());
        statusTv.setText("Previous : " + ttModel.getStatus());
        regionTv.setText("Previous : " + ttModel.getRegion());
        ticketTypeTv.setText("Previous : " + ttModel.getServiceType());


        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ttModel.setClientName(autoCompleteTextView.getText().toString().trim());
                ttModel.setTicketDetails(ticketDetailEt.getText().toString().trim());
                ttModel.setStatus(statusName);
                ttModel.setRegion(regionName);
                ttModel.setServiceType(serviceType);
                ttModel.setEttr(ticketEttrEt.getText().toString().trim());
                ttModel.setActionOwner(ownerName);

                if (statusName.equals(Constants.CLOSED)) {
                    ttModel.setClosingTime(SystemUtils.getTimeStamp());
                }

                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(Constants.TICKET_TABLE).child(ttModel.getId()).setValue(ttModel).
                        addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                Toast.makeText(SystemUtils.getActivity(), "Task Updated", Toast.LENGTH_SHORT).show();
//                                    list.set(clickedPosition, taskModel);
                                dialog.dismiss();
//                                    TaskListActivity.this.recreate();
                                adapter.notifyDataSetChanged();
                            }
                        });
            }
        });

        dialog.show();
    }

    void populateClients() {
        String[] fruits = {"HEC", "Farm Fresh Foods", "Bank Alhabib", "Traditions", "HBL",
                "Mari Petroleum", "CPHGC", "Bestway Cement", "HBL", "NHA", "MCB"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SystemUtils.getActivity(),
                R.layout.spinner_item, fruits);
        autoTextClient.setThreshold(1); //will start working from first character
        autoTextClient.setAdapter(adapter);
    }

    void focusedInputLayouts() {
        ticketDetailEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ticketDetailEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                } else
                    ticketDetailEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    autoCompleteTextView.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                } else
                    autoCompleteTextView.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (direction == ItemTouchHelper.LEFT) {
            if (userModel.isAdmin()) {
                showDialogForConfirmation(ticketsList.get(position));
            } else {
                Toast.makeText(SystemUtils.getActivity(), "Dear " + userModel.getName() + " You don't have rights to delete ticket", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            }
        } else if (direction == ItemTouchHelper.RIGHT) {
            adapter.notifyDataSetChanged();
            showDetailDialog(ticketsList.get(position));
        }
    }

    void showDialogForConfirmation(final TTModel ticketModel) {
        new AlertDialog.Builder(SystemUtils.getActivity())
                .setTitle("Delete Ticket")
                .setMessage("Do you really want to delete this ticket?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteNote(ticketModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void deleteNote(String id) {
        mDatabase.child(Constants.TICKET_TABLE).child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(SystemUtils.getActivity(), "Ticket Deleted !", Toast.LENGTH_SHORT).show();
//                        list.remove(clickedPosition);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    void performSearch(List<TTModel> list, String client) {
        List<TTModel> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getClientName().toLowerCase().contains(client)) {
                newList.add(list.get(i));
            }
        }
        crossIV.setVisibility(View.VISIBLE);
        setRecyclerView(newList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.crossIV:
//                setRecyclerView(ticketsList);
                break;
        }
    }
}

