package com.example.ttescalation.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.ttescalation.Model.CommentModel;
import com.example.ttescalation.Model.NotificationModel;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class AddTicketFragment extends Fragment implements View.OnClickListener {

    EditText ttTimeEt, ttDateEt , ticketDetailEt;

    AutoCompleteTextView autoTextView;

    Spinner typeSpinner, regionSpinner , statusSpinner;
    Button addBtn;

    String serviceType = "";
    String statusName = "";
    String regionName = "";
    String ownerName = "";

    DatePickerDialog.OnDateSetListener date;
    private DatabaseReference mDatabase;
    AVLoadingIndicatorView avi;
    UserModel userModel;

    Spinner actionOwnerSpinner;

    public AddTicketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_ticket, container, false);

        setXml(view);
        return view;
    }

    void setXml(View view) {
        ttTimeEt = view.findViewById(R.id.ttTimeEt);
        ttDateEt = view.findViewById(R.id.ttDateEt);
        ticketDetailEt = view.findViewById(R.id.ticketDetailEt);
        typeSpinner = view.findViewById(R.id.typeSpinner);
        regionSpinner = view.findViewById(R.id.regionSpinner);
        statusSpinner = view.findViewById(R.id.statusSpinner);
        autoTextView = view.findViewById(R.id.autoTextView);
        actionOwnerSpinner = view.findViewById(R.id.actionOwnerSpinner);
        addBtn = view.findViewById(R.id.addBtn);
        avi = view.findViewById(R.id.avi);

        ttDateEt.setOnClickListener(this);
        ttTimeEt.setOnClickListener(this);
        addBtn.setOnClickListener(this);

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        populateTypes();
        populateRegion();
        populateStatus();
        populateClients();
        populateActionOwners();

        focusedInputLayouts();
    }

    void populateTypes (){
        final List<String> serviceTypes = new ArrayList<>();
        serviceTypes.add("DIA");
        serviceTypes.add("DPLC");
        serviceTypes.add("PRI");
        serviceTypes.add("FTTH");
        serviceTypes.add("IPLC");
        serviceTypes.add("IPSec");
        serviceTypes.add("M2M");
        serviceTypes.add("M2M (Aggregation Down)");
        serviceTypes.add("SIP PRI");
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, serviceTypes);
        typeSpinner.setAdapter(adap);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceType = serviceTypes.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    void populateRegion (){
        final List<String> regions = new ArrayList<>();
        regions.add("North");
        regions.add("Central");
        regions.add("South");
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, regions);
        regionSpinner.setAdapter(adap);
        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                regionName = regions.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    void populateStatus(){
        final List<String> statuses = new ArrayList<>();
        statuses.add("Open");
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, statuses);
        statusSpinner.setAdapter(adap);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statusName = statuses.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    void populateClients (){
        String[] fruits = {"HEC", "Farm Fresh Foods", "Bank Alhabib", "Traditions", "HBL",
                "Mari Petroleum", "CPHGC", "Bestway Cement", "HBL" , "NHA" , "MCB"};

        ArrayAdapter<String> adapter = new ArrayAdapter <String> (SystemUtils.getActivity(),
                R.layout.spinner_item , fruits);
        autoTextView.setThreshold(1); //will start working from first character
        autoTextView.setAdapter(adapter);
    }
    void populateActionOwners (){
        final String[] owners = {Constants.VENDOR, Constants.NOMC , Constants.CUSTOMER};

        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, owners);
        actionOwnerSpinner.setAdapter(adap);

        actionOwnerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ownerName = owners[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setDigitalWatch() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog Tp = new TimePickerDialog(SystemUtils.getActivity(), android.R.style.Theme_Holo_Light_Dialog,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ttTimeEt.setText(hourOfDay + ":" + minute);

                    }
                }, hour, minute, false);
        Tp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Tp.show();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.ttDateEt:
                setDigitalDatePicker();
                break;
            case R.id.ttTimeEt:
                setDigitalWatch();
                break;
            case R.id.addBtn:
                generateTicket();
                break;
        }
    }

    void setDigitalDatePicker(){
        final Calendar myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                ttDateEt.setText(sdf.format(myCalendar.getTime()));
            }

        };
        new DatePickerDialog(SystemUtils.getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    void generateTicket (){
        String detail = ticketDetailEt.getText().toString().trim();
        String client = autoTextView.getText().toString().trim();

        if (detail.isEmpty()){
            Toast.makeText(SystemUtils.getActivity(), "Please enter some details about ticket ", Toast.LENGTH_LONG).show();
        }else if (client.isEmpty()){
            Toast.makeText(SystemUtils.getActivity(), "Please select client for ticket generation " , Toast.LENGTH_LONG).show();
        }else {
            startAnim();
            String currentDate = "";
            Date date = null;
            try {
                currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                date = (Date)formatter.parse(currentDate);
                System.out.println("Today_is " +date.getTime());
            }catch (Exception e){

            }

            List<CommentModel> comments = new ArrayList<>();
            final String key = mDatabase.push().getKey();
            TTModel ttModel = new TTModel(key, detail,date.getTime(),client,serviceType ,regionName,
                    Constants.OPEN , userModel.getName() , 0 , comments , "Awaited" , ownerName ,userModel.isAdmin());
            mDatabase.child(Constants.TICKET_TABLE).child(key).setValue(ttModel).
                    addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            stopAnim();

                            if (task.isSuccessful()) {
                                Toast.makeText(SystemUtils.getActivity(), "Ticket generated Successfully !", Toast.LENGTH_LONG).show();

                                String message = serviceType.toUpperCase() + " for " + regionName;

                                NotificationModel notificationData = new NotificationModel(userModel.getId() , message ,
                                        userModel.getName(), key, Constants.TICKET_CREATED);
                                DatabaseReference notiTable = FirebaseDatabase.getInstance().getReference("CommentNotifications");
                                notiTable.child(userModel.getId()).push().setValue(notificationData);

                                ticketDetailEt.getText().clear();
                                autoTextView.getText().clear();

                                navigateToHistoryFragment(Constants.ALL_REGIONS, "null");
                            } else if (task.isCanceled())
                                Toast.makeText(SystemUtils.getActivity(), "Some error occurred ", Toast.LENGTH_LONG).show();
                        }
                    });
        }

    }
    void navigateToHistoryFragment(String region, String status) {
        Bundle bundle = new Bundle();
        bundle.putString("Type", Constants.ALL_REGIONS);
        bundle.putString("Status", "null");
        bundle.putString("direction", Constants.DashboardSide);

        HistoryFragment historyFragment = new HistoryFragment();
        historyFragment.setArguments(bundle);

        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, historyFragment);
        transaction.commit();
        onDestroyView();
        onDestroy();
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    void focusedInputLayouts (){
        ticketDetailEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    ticketDetailEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    ticketDetailEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
        autoTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    autoTextView.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    autoTextView.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
    }
}
