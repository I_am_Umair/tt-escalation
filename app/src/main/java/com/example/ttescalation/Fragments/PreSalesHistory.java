package com.example.ttescalation.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ttescalation.Adapters.HistoryAdapter;
import com.example.ttescalation.Adapters.PreSalesHistoryAdapter;
import com.example.ttescalation.Adapters.SwipeRecyclerView.PreSalesRecyclerItemTouch;
import com.example.ttescalation.Adapters.SwipeRecyclerView.RecyclerItemTouchShelf;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.TaskModel;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.RecyclerViewHandler;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PreSalesHistory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreSalesHistory extends Fragment implements RecyclerItemTouchShelf.RecyclerItemTouchHelperListener, View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PreSalesHistory() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PreSalesHistory.
     */
    // TODO: Rename and change types and number of parameters
    public static PreSalesHistory newInstance(String param1, String param2) {
        PreSalesHistory fragment = new PreSalesHistory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    RecyclerView recyclerView;
    List<TaskModel> tasksList = new ArrayList<>();
    PreSalesHistoryAdapter adapter;
    private DatabaseReference mDatabase;
    AVLoadingIndicatorView avi;
    TextView noItemTv;
    UserModel userModel;
    PreSalesHistoryAdapter.RecyclerItemClickListener recyclerItemClickListener = null;
    String statusName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pre_sales_history, container, false);

        setXml(view);

        return view;
    }

    void setXml(View view) {
        avi = view.findViewById(R.id.avi);
        recyclerView = view.findViewById(R.id.recyclerView);
        noItemTv = view.findViewById(R.id.noItemTv);

        userModel = (UserModel) new SystemPrefs(SystemUtils.getActivity()).getOjectData(Constants.USER, UserModel.class);
        recyclerView = new RecyclerViewHandler().settingRecyleView(recyclerView, LinearLayout.VERTICAL, SystemUtils.getActivity());
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new PreSalesRecyclerItemTouch(0, ItemTouchHelper.LEFT
                | ItemTouchHelper.RIGHT, this, SystemUtils.getActivity());
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        recyclerItemClickListener = new PreSalesHistoryAdapter.RecyclerItemClickListener() {
            @Override
            public void onClick(int position, TaskModel datum, boolean showDetail, boolean delete) {
//                if (showDetail)
//                    showDetailDialog(datum);
//                else
                if (userModel.isAdmin())
                    showEditDialog(datum);
                else
                    Toast.makeText(SystemUtils.getActivity(), "You don't have rights to edit ticket", Toast.LENGTH_LONG).show();
            }
        };
        Bundle bundle = this.getArguments();
        if (bundle != null && !bundle.isEmpty()) {
//            String type = bundle.getString("Type");
            String status = bundle.getString("Status");
            getAllDatafromTasksTableByStatus(status);
        }
    }

    private void showEditDialog(final TaskModel taskModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.fragment_add_task);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Button addBtn = dialog.findViewById(R.id.addBtn);
        final EditText taskNameEt = dialog.findViewById(R.id.taskNameEt);
        final EditText taskDetailsEt = dialog.findViewById(R.id.taskDetailsEt);
        Spinner statusSpinner = dialog.findViewById(R.id.statusSpinner);
        final EditText actionOwnerEt = dialog.findViewById(R.id.actionOwnerEt);
        final AVLoadingIndicatorView avi = dialog.findViewById(R.id.avi);
        final FrameLayout backRl = dialog.findViewById(R.id.backRl);
        backRl.setBackground(SystemUtils.getActivity().getDrawable(R.drawable.popup_bg_white));
        backRl.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));



        final List<String> statuses = new ArrayList<>();
        statuses.add(Constants.OPEN);
        statuses.add(Constants.CLOSED);
        SpinnerAdapter adap = new ArrayAdapter<String>(SystemUtils.getActivity(), R.layout.spinner_item, statuses);
        statusSpinner.setAdapter(adap);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusName = statuses.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        taskNameEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    taskNameEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    taskNameEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });
        taskDetailsEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    taskDetailsEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    taskDetailsEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });

        actionOwnerEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    actionOwnerEt.setBackground(getResources().getDrawable(R.drawable.edittext_higlighted));
                }else
                    actionOwnerEt.setBackground(getResources().getDrawable(R.drawable.edittext_bg));
            }
        });

        taskNameEt.setText(taskModel.getName());
        taskDetailsEt.setText(taskModel.getDetails());
        actionOwnerEt.setText(taskModel.getOwner());
//        taskNameEt.setText(taskModel.getName());

        if (taskModel.getStatus() != null) {
            switch (taskModel.getStatus()) {
                case Constants.OPEN:
                    statusSpinner.setSelection(0);
                    break;
                case Constants.CLOSED:
                    statusSpinner.setSelection(1);
                    break;
                default:
                    statusSpinner.setSelection(0);
                    break;
            }
        }

        addBtn.setText("Edit Task");
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = taskNameEt.getText().toString().trim();
                String details = taskDetailsEt.getText().toString().trim();
                String currentStatus = statusName.trim();
                String owner = actionOwnerEt.getText().toString().trim();

                if (name.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter Task name !", Toast.LENGTH_LONG).show();
                }else if (details.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter some Task details !", Toast.LENGTH_LONG).show();
                }else if (currentStatus.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please enter current status of task !", Toast.LENGTH_LONG).show();
                }else if (owner.isEmpty()){
                    Toast.makeText(SystemUtils.getActivity(), "Please mention Action owner for this task !", Toast.LENGTH_LONG).show();
                }else {

                    avi.setVisibility(View.VISIBLE);
                    avi.show();

                    taskModel.setName(name);
                    taskModel.setDetails(details);
                    taskModel.setStatus(currentStatus);
                    taskModel.setOwner(owner);

                    if (currentStatus.equals(Constants.CLOSED)) {
                        taskModel.setClosingTime(SystemUtils.getTimeStamp());
                    }

                    mDatabase = FirebaseDatabase.getInstance().getReference();
                    mDatabase.child(Constants.TASK_TABLE).child(taskModel.getId()).setValue(taskModel).
                            addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    avi.hide();
                                    avi.setVisibility(View.GONE);
                                    if (task.isSuccessful()) {
                                        Toast.makeText(SystemUtils.getActivity(), "Task Updated", Toast.LENGTH_SHORT).show();
//                                    list.set(clickedPosition, taskModel);
                                        dialog.dismiss();
//                                    TaskListActivity.this.recreate();
                                        adapter.notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(SystemUtils.getActivity(), "Some error occurred", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
        dialog.show();
    }

    void getAllDatafromTasksTableByStatus(final String status) {
        startAnim();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.TASK_TABLE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tasksList = new ArrayList<>();
                stopAnim();
                Log.d("counts", dataSnapshot.getChildrenCount() + "");

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TaskModel listdata = dataSnapshot1.getValue(TaskModel.class);
                    if (status != "Total") {
                        if (listdata.getStatus().equals(status)) {
                            tasksList.add(listdata);
                        }
                    } else {
                        tasksList.add(listdata);
                    }
                }
                setRecyclerView(tasksList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
                stopAnim();
            }
        });
    }

    void setRecyclerView(List<TaskModel> list) {
        if (list.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new PreSalesHistoryAdapter(list, SystemUtils.getActivity(), recyclerItemClickListener);
            recyclerView.setAdapter(adapter);
            noItemTv.setVisibility(View.GONE);

        } else {
            recyclerView.setVisibility(View.GONE);
            noItemTv.setVisibility(View.VISIBLE);
        }
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.hide();
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    public void onPause() {
        stopAnim();
        super.onPause();
    }

    @Override
    public void onStop() {
        stopAnim();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (direction == ItemTouchHelper.LEFT) {
            if (userModel.isAdmin()) {
                showDialogForConfirmation(tasksList.get(position));
            } else {
                Toast.makeText(SystemUtils.getActivity(), "Dear " + userModel.getName() + " You don't have rights to delete ticket", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            }
        } else if (direction == ItemTouchHelper.RIGHT) {
            adapter.notifyDataSetChanged();
            showDetailDialog(tasksList.get(position));
        }
    }
    void showDialogForConfirmation(final TaskModel ticketModel) {
        new AlertDialog.Builder(SystemUtils.getActivity())
                .setTitle("Delete Task")
                .setMessage("Do you really want to delete this task?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteNote(ticketModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void deleteNote(String id) {
        mDatabase.child(Constants.TASK_TABLE).child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(SystemUtils.getActivity(), "Task Deleted !", Toast.LENGTH_SHORT).show();
//                        list.remove(clickedPosition);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    void performSearch(List<TaskModel> list, String client) {
        List<TaskModel> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().toLowerCase().contains(client)) {
                newList.add(list.get(i));
            }
        }
//        crossIV.setVisibility(View.VISIBLE);
        setRecyclerView(newList);
    }

    void showDetailDialog(TaskModel ttModel) {
        final Dialog dialog = new Dialog(SystemUtils.getActivity());
        dialog.setContentView(R.layout.pre_sales_detail_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        EditText taskIDEt = dialog.findViewById(R.id.taskIDEt);
        EditText taskNameEt = dialog.findViewById(R.id.taskNameEt);
        EditText taskDetailsEt = dialog.findViewById(R.id.taskDetailsEt);
        EditText dateTimeEt = dialog.findViewById(R.id.dateTimeEt);
        EditText ownerEt = dialog.findViewById(R.id.ownerEt);
        EditText taskStatusEt = dialog.findViewById(R.id.taskStatusEt);


        taskIDEt.setText(ttModel.getId());
        taskNameEt.setText(ttModel.getName());
        dateTimeEt.setText(SystemUtils.getDateFromTimeStamp(ttModel.getStartTime()));
        taskDetailsEt.setText(ttModel.getDetails());
        ownerEt.setText(ttModel.getOwner());
        taskStatusEt.setText(ttModel.getStatus());

        dialog.show();

    }

}