package com.example.ttescalation.SendNotificationPack;

public class Token {
    private String token , id;

    public Token(String token , String id) {
        this.token = token;
        this.id = id;
    }

    public Token() {
    }

    public String getToken() {
        return token;
    }
    public String getid() {
        return id;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public void setid(String id) {
        this.id = id;
    }
}
