package com.example.ttescalation.SendNotificationPack;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.ttescalation.Activities.CommentsActivity;
import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Model.UserModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemPrefs;
import com.example.ttescalation.Utils.SystemUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MyFireBaseMessagingService extends FirebaseMessagingService {
    String title, message;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("jsonReceived", remoteMessage.getData().toString());

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            sendOreoNotification(remoteMessage);
//        } else {
        UserModel userModel = (UserModel) new SystemPrefs(this.getApplicationContext()).getOjectData(Constants.USER, UserModel.class);
        if (userModel.getId() != null && !userModel.getId().equals(remoteMessage.getData().get("senderId"))) {
            sendNotification(remoteMessage);
        }
//        }
    }

    private void sendOreoNotification(RemoteMessage remoteMessage) {

        String ticketId = remoteMessage.getData().get("ticketId");
        String icon = remoteMessage.getData().get("icon");
        String title = "New Comment from " + remoteMessage.getData().get("name");
        ;
        String body = remoteMessage.getData().get("message");

        RemoteMessage.Notification notification = remoteMessage.getNotification();
//            int j = Integer.parseInt(ticketId);
        Intent intent = new Intent(this, CommentsActivity.class);
        intent.putExtra("id", ticketId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        OreoNotification oreoNotification = new OreoNotification(this);
        Notification.Builder builder = oreoNotification.getOreoNotification(title, body, pendingIntent,
                defaultSound, icon);

//            int i = 0;
//            if (j > 0) {
//                i = j;
//            }

        oreoNotification.getManager().notify(0, builder.build());

    }

    private void sendNotification(RemoteMessage remoteMessage) {

        String ticketId = remoteMessage.getData().get("ticketId");
        String icon = remoteMessage.getData().get("icon");
        String type = remoteMessage.getData().get("type");
        String title = "";
        if (type.equals(Constants.TICKET_CREATED))
            title = "New Ticket generated from " + remoteMessage.getData().get("name");
        else if (type.equals(Constants.TICKET_TABLE))
             title = "New Comment from " + remoteMessage.getData().get("name");

        String body = remoteMessage.getData().get("message");

        notifyThis(title, body, ticketId, type);

//            RemoteMessage.Notification notification = remoteMessage.getNotification();
//            int requestID = (int) System.currentTimeMillis();
////            int j = Integer.parseInt(ticketId.replaceAll("[\\D]", ""));
//            Intent intent = new Intent(this, CommentsActivity.class);
//            intent.putExtra("id" , ticketId);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID, intent, PendingIntent.FLAG_ONE_SHOT);
//
//            Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.drawable.zong_logo)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setAutoCancel(true)
//                    .setSound(defaultSound)
//                    .setContentIntent(pendingIntent);
//            NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
////            int i = 0;
////            if (j > 0) {
////                i = j;
////            }
//
//            noti.notify(1, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void notifyThis(String title, String message, String id, String type) {
        Log.d("idGot", id);

        Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);

        if (type.equals(Constants.TICKET_CREATED))
            intent = new Intent(getApplicationContext(), DrawerActivity.class);

        intent.putExtra("id", id);
        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String CHANNEL_ID = "TTEscalation";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_LOW);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationManager.createNotificationChannel(notificationChannel);

            builder = new Notification.Builder(getApplicationContext(), CHANNEL_ID);
        } else {
            builder = new Notification.Builder(getApplicationContext());
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);

        if (new SystemPrefs(this).isMusic())
            playNotificationSound();

        Notification notification = builder
                .setContentText(message)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.zong_logo, "New Comment", pendingIntent)
                .setSmallIcon(R.drawable.zong_logo)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_LIGHTS)
//                .setSound(Notification.DEFAULT_SOUND)
//                .setVibrate(new long[]{0, 500, 1000})
//                .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/"+ R.raw.swiftly))
//                .setOnlyAlertOnce(true)
//                .setWhen(System.currentTimeMillis())
                .build();

//        notification.flags = Notification.DEFAULT_LIGHTS;
//        notification.defaults |= Notification.DEFAULT_LIGHTS;
//        notification.defaults = Notification.DEFAULT_VIBRATE;
//        notification.defaults = Notification.DEFAULT_SOUND;

        notificationManager.notify(createID(), notification);
        stopNotificationSound();
    }

    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/" + R.raw.swiftly);
            Ringtone r = RingtoneManager.getRingtone(this, alarmSound);
            r.play();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/" + R.raw.swiftly);
            Ringtone r = RingtoneManager.getRingtone(this, alarmSound);
            r.stop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public int createID(){
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
        return id;
    }

}
