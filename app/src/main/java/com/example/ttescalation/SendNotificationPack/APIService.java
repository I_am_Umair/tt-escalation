package com.example.ttescalation.SendNotificationPack;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAVcRcWfI:APA91bEiHP9siPPwHlfh3P4kjUqOG1GcvHETw7AgWEYXwYhDTY3lUKCHfk1gX4xAXFUaOxufG940FfbKKhq9pe9FfH3DKaAieT-cv3536zBgf1U-IVkP2bXPq0amESd-uXfH_c0DHC0Q" // Your server key refer to video for finding your server key
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotifcation(@Body NotificationSender body);
}

