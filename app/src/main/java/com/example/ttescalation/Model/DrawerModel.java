package com.example.ttescalation.Model;

import android.graphics.drawable.Drawable;

public class DrawerModel {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getPicture() {
        return picture;
    }

    public void setPicture(Drawable picture) {
        this.picture = picture;
    }

    String name ;
    Drawable picture;
}
