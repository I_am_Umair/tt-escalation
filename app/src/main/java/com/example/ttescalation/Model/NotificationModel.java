package com.example.ttescalation.Model;

public class NotificationModel {
    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NotificationModel(String senderId, String message, String name , String ticketId , String type) {
        this.senderId = senderId;
        this.message = message;
        this.name = name;
        this.ticketId = ticketId;
        this.type = type;
    }
    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }


    public NotificationModel (){}

    String senderId;
    String message;
    String name;
    String ticketId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;
}
