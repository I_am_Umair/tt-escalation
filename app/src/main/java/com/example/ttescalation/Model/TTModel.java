package com.example.ttescalation.Model;

import java.util.List;

public class TTModel {
    public TTModel(String id, String ticketDetails, long timeStamp, String clientName,
                   String serviceType, String region, String status , String creator ,
                   long closingTime , List<CommentModel> comments , String ettr , String actionOwner ,
                   boolean isAdmin) {
        this.id = id;
        this.ticketDetails = ticketDetails;
        this.timeStamp = timeStamp;
        this.clientName = clientName;
        this.serviceType = serviceType;
        this.region = region;
        this.status = status;
        this.creator = creator;
        this.closingTime = closingTime;
        this.comments = comments;
        this.ettr = ettr;
        this.actionOwner = actionOwner;
        this.isAdmin = isAdmin;
    }
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTicketDetails() {
        return ticketDetails;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getClientName() {
        return clientName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getRegion() {
        return region;
    }

    public String getStatus() {
        return status;
    }
    public TTModel (){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTicketDetails(String ticketDetails) {
        this.ticketDetails = ticketDetails;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    public String getEttr() {
        return ettr;
    }

    public void setEttr(String ettr) {
        this.ettr = ettr;
    }

    public String getActionOwner() {
        return actionOwner;
    }

    public void setActionOwner(String actionOwner) {
        this.actionOwner = actionOwner;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    String id;
    String ticketDetails ;
    long timeStamp;
    String clientName;
    String serviceType;
    String region;
    String status;
    String creator;
    long closingTime;
    List<CommentModel> comments;
    String ettr;
    String actionOwner;
    boolean isAdmin;



}
