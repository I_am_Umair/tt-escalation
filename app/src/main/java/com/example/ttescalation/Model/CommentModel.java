package com.example.ttescalation.Model;

public class CommentModel {

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public CommentModel(String senderId, String senderName, String message, long timeStamp) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.message = message;
        this.timeStamp = timeStamp;
    }
    public CommentModel(){}

    String senderId,senderName,message;
    long timeStamp;

}
