package com.example.ttescalation.Model;

import java.util.List;

public class TaskModel {


    public TaskModel (){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public long getClosingTime() {
        return closingTime;
    }
    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }
    public List<CommentModel> getComments() {
        return comments;
    }
    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    public TaskModel(String id,String name, String details, String status, String owner, long startTime , long closingTime , List<CommentModel> comments) {
        this.id = id;
        this.name = name;
        this.details = details;
        this.status = status;
        this.owner = owner;
        this.startTime = startTime;
        this.closingTime = closingTime;
        this.comments = comments;
    }

    String id , name, details, status , owner;
    long startTime;
    long closingTime;
    List<CommentModel> comments;
}
