package com.example.ttescalation.Model;

public class UserModel {

    public UserModel(String id, String email, String password , boolean isAdmin , String name , String designation , String phone , boolean isTyping) {
        this.id = id ;
        this.email = email;
        this.password = password;
        this.isAdmin = isAdmin;
        this.name = name;
        this.designation = designation;
        this.phone = phone;
        this.isTyping = isTyping;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    String email;
    String password;
    boolean isAdmin;
    String designation;
    String id;
    String name;
    String phone;
    boolean isTyping;

    public boolean isTyping() {
        return isTyping;
    }

    public void setTyping(boolean typing) {
        isTyping = typing;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }


    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    public UserModel(){};
}
