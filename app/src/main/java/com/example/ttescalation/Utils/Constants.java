package com.example.ttescalation.Utils;

public class Constants {

    public static String IS_LOGIN = "isLogin";
    public static String IS_Music = "isMusic";
    public static String USER = "user";
    public static String COMPLETE = "Complete";
    public static String IN_PROGRESS = "In Progress";
    public static String PENDING = "Pending";
    public static final String ALL = "All";
    public static final String OPEN = "Open";
    public static final String CLOSED = "Closed";
    public static final String NORTH = "North";
    public static final String SOUTH = "South";
    public static final String CENTRAL = "Central";
    public static final String ALL_REGIONS = "All Regions";

    public static final String TICKET_CREATED= "Ticket_created";
    public static final String TICKET_TABLE= "Tickets";
    public static final String TASK_TABLE= "Tasks";
    public static final String TICKET_TOKENS= "Tokens";
    public static final String USERS_TABLE= "Users";

    public static final String NOMC = "NOMC";
    public static final String CUSTOMER = "Customer";
    public static final String VENDOR = "Vendor";

    public static final String DIA = "DIA";
    public static final String DPLC = "DPLC";
    public static final String PRI = "PRI";
    public static final String FTTH = "FTTH";
    public static final String IPLC = "IPLC";
    public static final String IPSEC = "IPSec";
    public static final String M2M = "M2M";
    public static final String M2M_A = "M2M (Aggregation Down)";
    public static final String SIP_PRI = "SIP PRI";

    public static final String DashboardSide = "DashboardSide";
    public static final String FragmentSide = "FragmentSide";
//    public static boolean addToken = false;

}
