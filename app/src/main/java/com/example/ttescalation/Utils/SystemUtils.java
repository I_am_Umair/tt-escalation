package com.example.ttescalation.Utils;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Vibrator;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class SystemUtils {
    private static Activity activityData;
    private static boolean status;



    public static void setActivity(Activity activity){
        activityData = activity;
    }
    public  static Activity getActivity(){
        return activityData;
    }
    public static boolean isActivityVisible(){
        return status;
    }
    public static void setActivityResume(){
        status = true;
    }
    public static void setActivityPause(){

//        PackageManager packageManager = SystemUtils.getActivity().getPackageManager();
//        try {
//            ActivityInfo info = packageManager.getActivityInfo(SystemUtils.getActivity().getComponentName(), 0);
//            Log.e("ActivitynamePaused:",   info.name);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
        status = false;
    }



    /**
     * Hide Keyboard
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static  void vibrateMoible(Activity activity) {
        AudioManager am = (AudioManager)activity.getSystemService(Context.AUDIO_SERVICE);
        Vibrator vibe;
        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                Log.i("MyApp", "Silent mode");
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                vibe = (Vibrator)activity. getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(100);
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                vibe = (Vibrator)activity. getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(100);
                break;
        }
    }

//    public static void showSuccessToast(String message,Activity activity){
//        Log.d("statusValue", String.valueOf(status));
//        if (isActivityVisible()) {
//            LayoutInflater inflater = activity.getLayoutInflater();
//            View layout = inflater.inflate(R.layout.success_toast_layout, null);
//            TextView msg = layout.findViewById(R.id.toast_message);
//            msg.setText(message);
//            Toast toast = new Toast(activity);
//            toast.setDuration(Toast.LENGTH_LONG);
//            toast.setView(layout);
//            toast.show();
//        }
//    }
//    public static void showErrorToast(String message,Activity activity){
//        if (isActivityVisible()) {
//            LayoutInflater inflater = activity.getLayoutInflater();
//            View layout = inflater.inflate(R.layout.error_toast_layout,
//                    null);
//            TextView msg = layout.findViewById(R.id.toast_message);
//            msg.setText(message);
//            Toast toast = new Toast(activity);
//            toast.setDuration(Toast.LENGTH_LONG);
//            toast.setView(layout);
//            toast.show();
//        }
//    }

    public static String getDateFromTimeStamp(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000L);
        String date = DateFormat.format("dd-MM-yyyy HH:mm:ss", cal).toString();
        return date;
    }
    public static String getDateAndTimeFromTimeStamp(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatter.format(time);
    }


    public static boolean verifyEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
    public static long getTimeStamp() {
        Date date = null;
        String currentDate = "";

        try {
            currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
            java.text.DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            date = (Date) formatter.parse(currentDate);
            System.out.println("Today_is " + date.getTime());
        } catch (Exception e) {
            Log.d("Exception Occured", e.getMessage());
        }
        return date.getTime();
    }
}
