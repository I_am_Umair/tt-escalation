package com.example.ttescalation.Utils;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by MME on 6/8/2018.
 */

public class RecyclerViewHandler {
    public RecyclerView settingRecyleView(RecyclerView recyclerView , int type, Context context){
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context,
                type, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemViewCacheSize(40);
        return  recyclerView;
    }
}
