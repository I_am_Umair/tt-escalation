package com.example.ttescalation.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ttescalation.Activities.CommentsActivity;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.Model.TaskModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PreSalesHistoryAdapter extends RecyclerView.Adapter<PreSalesHistoryAdapter.MyViewHolder> {
    private List<TaskModel> list;
    private Context context;
    List<TaskModel> itemsCopy;
    RecyclerItemClickListener recyclerItemClickListener;

    public PreSalesHistoryAdapter(List<TaskModel> list, Context context, RecyclerItemClickListener recyclerItemClickListener) {
        this.list = list;
        this.itemsCopy = list;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    @Override
    public PreSalesHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_history_layout, parent, false);
        return new PreSalesHistoryAdapter.MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final PreSalesHistoryAdapter.MyViewHolder holder, final int position) {
        final TaskModel item = list.get(position);

        holder.descriptionTv.setText(item.getDetails());
        holder.dateTv.setText(getDate(item.getStartTime()));
        holder.statusTv.setText(item.getOwner());
        if (item.getStatus().equals(Constants.OPEN)) {
            holder.mintsTv.setText(SystemUtils.getDateDiff(new Date(item.getStartTime()),
                    new Date(SystemUtils.getTimeStamp()), TimeUnit.MINUTES) + "");
        }
        holder.idTv.setText(item.getName());
        holder.allotedTv.setText(item.getName());
        holder.ownerTv.setText("Owner : " + item.getOwner());

        if (item.getStatus().equals(Constants.CLOSED)) {
            holder.view_foreground.setCardBackgroundColor(SystemUtils.getActivity().getColor(R.color.openColor_light));
            holder.resolTv.setVisibility(View.VISIBLE);
            holder.resolTv.setText("R.T : " + SystemUtils.getDateDiff(new Date(item.getStartTime()),
                    new Date(item.getClosingTime()), TimeUnit.DAYS) + "");

            holder.mintsTv.setText(SystemUtils.getDateDiff(new Date(item.getStartTime()),
                    new Date(item.getClosingTime()), TimeUnit.DAYS) + "");

            holder.resDateTv.setVisibility(View.VISIBLE);
            holder.resDateTv.setText(getDate(item.getClosingTime()));

        }

        holder.editIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerItemClickListener.onClick(holder.getAdapterPosition(), item, false, false);
            }
        });
        holder.chatIv.setVisibility(View.GONE);
        holder.chatIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.getActivity().startActivity(new Intent(SystemUtils.getActivity(), CommentsActivity.class)
                        .putExtra("id", item.getId())
                .putExtra("type" , Constants.TASK_TABLE));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionTv, dateTv, statusTv, allotedTv, mintsTv, idTv, resolTv, resDateTv, ownerTv;
        ImageView editIv, deleteIv, chatIv;
        public CardView view_foreground;

        //        CardView view_forground;
        public MyViewHolder(View view) {
            super(view);
            descriptionTv = view.findViewById(R.id.descriptionTv);
            idTv = view.findViewById(R.id.idTv);
            dateTv = view.findViewById(R.id.dateTv);
            statusTv = view.findViewById(R.id.statusTv);
            allotedTv = view.findViewById(R.id.allotedTv);
            editIv = view.findViewById(R.id.editIv);
            resDateTv = view.findViewById(R.id.resDateTv);
            deleteIv = view.findViewById(R.id.deleteIv);
            mintsTv = view.findViewById(R.id.mintsTv);
            resolTv = view.findViewById(R.id.resolTv);
            chatIv = view.findViewById(R.id.chatIv);
            view_foreground = view.findViewById(R.id.view_foreground);
            ownerTv = view.findViewById(R.id.ownerTv);
        }
    }


    public interface RecyclerItemClickListener {
        void onClick(int position, TaskModel datum, boolean showDetail, boolean delete);
    }

    private String getDate(long time_stamp_server) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YY HH:mm");
        return formatter.format(time_stamp_server);
    }

}