package com.example.ttescalation.Adapters.SwipeRecyclerView;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Canvas;
import android.view.View;


import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ttescalation.Activities.DrawerActivity;
import com.example.ttescalation.Adapters.HistoryAdapter;
import com.example.ttescalation.Adapters.PreSalesHistoryAdapter;
import com.example.ttescalation.Fragments.HistoryFragment;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.SystemUtils;


public class RecyclerItemTouchShelf extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;
    Context context;

    public RecyclerItemTouchShelf(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener, Context context) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
        this.context = context;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            View foregroundView = null;
            foregroundView = ((HistoryAdapter.MyViewHolder) viewHolder).view_foreground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        View foregroundView = null;
        foregroundView = ((HistoryAdapter.MyViewHolder) viewHolder).view_foreground;

        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        View foregroundView = null;

        foregroundView = ((HistoryAdapter.MyViewHolder) viewHolder).view_foreground;

        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        View foregroundView = null;
        foregroundView = ((HistoryAdapter.MyViewHolder) viewHolder).view_foreground;

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}