package com.example.ttescalation.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.ttescalation.Activities.CommentsActivity;
import com.example.ttescalation.Model.CommentModel;
import com.example.ttescalation.Model.TTModel;
import com.example.ttescalation.R;
import com.example.ttescalation.Utils.Constants;
import com.example.ttescalation.Utils.SystemUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    private List<CommentModel> list;
    private Context context;

    HistoryAdapter.RecyclerItemClickListener recyclerItemClickListener;

    public CommentAdapter(List<CommentModel> list, Context context ) {
        this.list = list;

        this.context = context;

    }

    @Override
    public CommentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_history_layout, parent, false);
        return new CommentAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CommentAdapter.MyViewHolder holder, final int position) {
        final CommentModel item = list.get(position);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionTv, dateTv, statusTv, allotedTv , mintsTv , idTv , resolTv;
        ImageView editIv , deleteIv , chatIv;

        //        CardView view_forground;
        public MyViewHolder(View view) {
            super(view);
            descriptionTv = view.findViewById(R.id.descriptionTv);
            idTv = view.findViewById(R.id.idTv);
            dateTv = view.findViewById(R.id.dateTv);
            statusTv = view.findViewById(R.id.statusTv);
            allotedTv = view.findViewById(R.id.allotedTv);
            editIv= view.findViewById(R.id.editIv);
            deleteIv= view.findViewById(R.id.deleteIv);
            mintsTv= view.findViewById(R.id.mintsTv);
            resolTv= view.findViewById(R.id.resolTv);
            chatIv= view.findViewById(R.id.chatIv);
        }
    }


    public interface RecyclerItemClickListener {
        void onClick(int position, TTModel datum , boolean showDetail , boolean delete);
    }
    private String getDate(long time_stamp_server) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(time_stamp_server);
    }

}