package com.example.ttescalation.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.ttescalation.Model.DrawerModel;
import com.example.ttescalation.R;

import java.util.ArrayList;
import java.util.List;

public class DrawerAdapter extends BaseAdapter {
    private Context mContext;
    List<DrawerModel> mlist = new ArrayList<>();


    // Constructor
    public DrawerAdapter(List<DrawerModel> list, Context c){
        mContext = c;
        mlist= list;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        TextView itemName = null;
        ImageView imageView = null;
        LinearLayout linearLayout = null;

        if (convertView == null)
        {
            gridView = new View(mContext);
            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.drawer_list_view_layout, null);
            itemName = gridView.findViewById(R.id.itemName);
            imageView = gridView.findViewById(R.id.imageView);
            linearLayout = gridView.findViewById(R.id.linearLayout);

        }
        else
        {
            gridView = (View) convertView;
            imageView = gridView.findViewById(R.id.imageView);
            itemName = gridView.findViewById(R.id.itemName);
            linearLayout = gridView.findViewById(R.id.linearLayout);

        }

        itemName.setText(mlist.get(position).getName());

        imageView.setImageDrawable(mlist.get(position).getPicture());

        return gridView;
    }

}